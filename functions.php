<?php

function publicAPIMethods()
{
	return array('addPage','delPage','getPage');
}


function addPage()
{
	
	$type = trim($_REQUEST['type']);
	$params = array();
	
	$data['parent'] = (int)$_REQUEST['parent'];
	$data['type'] = $_REQUEST['type'];
	$data['alias'] = $_REQUEST['alias'];
	$data['header'] = $_REQUEST['header'];
	
	if (!class_exists($type))
		return array('error'=>'type error');
	
	$Type = new $type(false);
	if ($idtype = $Type->add($data))
	{
		return array('success'=>'add page '.$idtype,'idtype'=>$idtype);
	}
	else
	{
		return false;
	}
	
}
