<?php

//подключение модулей админки и методов модулей
//обработка запросов к апи

class ControlAPI
{
	
	private $methods = array(); //массив методов
	private $menumodules; //структура меню (из конфига). Список модулей
	private $menusections; //структура меню (из конфига). Список секций
	
	public $modules = array(); //массив загруженных и разрешенных модулей
	public $сontrollers = array(); //массив контроллеров
	
	static $errors;
	
	function __construct()
	{
		
		//параметры панели управления: список модулей и прав на доступ к ним
		include(ROOT."/control.cfg.php");
		
		//отбивка неавторизованных пользователей или не перечисленных в массиве $controlaccess
		if (!User::$id || !isset($controlaccess[User::$id]))
		{
			Errors::set('Guest access denied');
			$this->access = false;
			return false;
		}
		$this->access = true;
		
		if (!self::$errors)
			self::$errors = array();
		
		//??
		$_REQUEST['limit'] = (int)$_REQUEST['limit']?(int)$_REQUEST['limit']:20;
		
		
		//загрузка файла общих функций
		include_once(dirname(__FILE__).'/functions.php');
		//регистрация общих методов
		foreach(publicAPIMethods() as $method)
			$this->methods[] = $method;
		
		
		
		//подключение модулей
		
		$this->menumodules = $menumodules;
		$this->menusections = $menusections;
		
		$permittedModules = explode(',',$controlaccess[User::$id]);
		$permittedModules[] = 'default';
		
		foreach($permittedModules as $module)
		{
			$moduleFile = dirname(__FILE__).'/modules/'.$module.'.php';
			if (file_exists($moduleFile))
			{
				
				include_once($moduleFile);
				
				$moduleName = $module.'ModuleName';
				$controlAPIMethods = $module.'ControlAPIMethods';
				$additionalControllers = $module.'AdditionalControllers';
				
				//регистрация модулей
				if (function_exists($moduleName))
				{
					$this->modules[$module]=array('alias'=>$module,'header'=>$moduleName());
				}
				
				//регистрация контроллеров
				$this->сontrollers[] = $module;
				if (function_exists($additionalControllers))
				{
					foreach($additionalControllers() as $c)
						$this->сontrollers[] = $c;
				}
				
				//регистрация методов модуля
				if (function_exists($controlAPIMethods))
				{
					$methods = $controlAPIMethods();
					foreach($methods as $method)
						$this->methods[] = $method;
				}
			}
		}
		
	}
	
	//вызов метода API
	function __call($name,$arguments = array())
	{
		//pr($this->methods);
		if (in_array($name,$this->methods))
		{
			return $name();
		}
		else
		{
			self::setError('method '.$name.' not found');
			return false;
		}
	}
	
	//регистрация метода
	function registerMethod($method,$module)
	{
		if (isset($this->methods[$method]) || method_exists($this,$method))
		{
			echo 'ошибка добавления метода, имя "'.$method.'" занято';
			exit;
		}
		$this->methods[$method] = $module;
	}
	
	
	function getMenu()
	{
		
		$tmpmenu = array();
		
		
		foreach($this->modules as $module)
		{
			
			$section = $this->menumodules[$module['alias']];
			
			if (!isset($tmpmenu[$section]))
				$tmpmenu[$section] = $this->menusections[$section];
			
			$tmpmenu[$section]['modules'][] = $module;
			
		}
		
		
		//порядок секций в соответствии с конфигом
		$menu = array();
		foreach($this->menusections as $section=>$params)
		{
			if (isset($tmpmenu[$section]))
				$menu[] = $tmpmenu[$section];
		}
		
		
		return $menu;
		
	}
	
	
	function getControllers()
	{
		return $this->сontrollers;
	}
	
	function getModules()
	{
		return $this->modules;
	}
	
	
	function getSystemInfo()
	{
		$info = array();
		$info['sitename'] = SITENAME;
		$info['uri'] = URI;
		
		return $info;
	}
	
	function getMethods()
	{
		return $this->methods;
	}
	
	
	function getTypes()
	{
		$Types = new Types();
		return $Types->getAllTypeNames();
	}
	
	
	static function setError($txt)
	{
		self::$errors[] = $txt;
	}
	
	
	static function getErrors()
	{
		
		$errors = array_merge(Errors::get(), self::$errors);
		
		if(count($errors))
			return $errors;
		
		return array('error');
		
	}
	
	
}
