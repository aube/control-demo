<?php
//api панели управления

include_once(dirname(__FILE__).'/../init.php');


if (!isset($_REQUEST['action']))
{
	$_REQUEST = file_get_contents("php://input");
	$_REQUEST = json_decode($_REQUEST,1);
}

if (!isset($_REQUEST['action']))
	exit;

$action = $_REQUEST['action'];

//подготовка общих параметров - фильтры, номер страницы, кол-во на листе 
if (isset($_REQUEST['filter']))
{
	
	$_REQUEST['filter'] = json_decode($_REQUEST['filter'],1);
	
	if (isset($_REQUEST['filter']['page']))
	{
		$_REQUEST['page'] = $_REQUEST['filter']['page'];
		unset($_REQUEST['filter']['page']);
	}
	
	if (isset($_REQUEST['filter']['limit']))
	{
		$_REQUEST['limit'] = $_REQUEST['filter']['limit'];
		unset($_REQUEST['filter']['limit']);
	}
	
	if (!isset($_REQUEST['page']))
		$_REQUEST['page'] = 1;
	
	$_REQUEST['page'] = $_REQUEST['page']-1;
	
	if (!isset($_REQUEST['limit']))
		$_REQUEST['limit'] = 20;
	
}


include_once(dirname(__FILE__).'/class.controlapi.php');
$API = new ControlAPI();

if ($API->access === false)
{
	Headers::error();
	Headers::json();
	echo json_encode(array('redirect'=>CONTROL_URL));
	exit;
}

$data = $API->$action();

if ($data === false)
{
	Headers::error();
	Headers::json();
	echo json_encode(ControlAPI::getErrors());
	exit;
}



if (is_array($data)){
	//отдаем результат в csv
	if ($_GET['format']=='csv' && isset($data['rows'])){
		header('Content-type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.time().'.csv');
		echo arr2csv($data['rows']);
		
	//отдаем результат в jsonp
	}elseif (isset($_GET['jsonp'])){
		header('Cache-Control: private', true);
		header('Content-type: text/javascript; charset=utf-8');
		header("Connection: close", true);
		echo ''.$_GET['jsonp'].'('.json_encode($data).')';
		
	//json
	}else{
		header('Content-type: application/json');
		//~ if (
			//~ !isset($data['success']) &&
			//~ !isset($data['errors']) &&
			//~ !isset($data['messages'])
		//~ )
			//~ $data['success'] = $action.': данные получены';
		
		echo json_encode($data);
	}
}
