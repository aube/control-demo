<!DOCTYPE html>
<html ng-app='controlApp'>

	<head>
		
		<title>Control Panel</title>
		
		<meta name="generator" content="LS-control"/>
		<meta name="author" content="aube.work"/>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<script type='text/javascript' src='./bower_components/tinymce/tinymce.min.js'></script>
		<script type="text/javascript" src="./_js/jquery.min.js"></script>
		<script type="text/javascript" src="./_js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="./_js/bootstrap.min.js"></script>
		<script type='text/javascript' src='./_js/chartist.min.js'></script>
		<script type="text/javascript" src="./_js/angular.min.js"></script>
		<script type="text/javascript" src="./_js/angular.route.js"></script>
		<script type="text/javascript" src="./_js/angular.tinymce.js"></script>
		<script type="text/javascript" src="./_js/angular-file-upload.js"></script>
		<script type='text/javascript' src='./_js/bootstrap-datepicker.min.js'></script>
		<script type='text/javascript' src='./_js/bootstrap-datepicker.ru.min.js'></script>
		
		<link rel="stylesheet" type="text/css" href="./_css/whhg.css"/>
		<link rel="stylesheet" type="text/css" href="./_css/chartist.min.css"/>
		<link rel="stylesheet" type="text/css" href="./_css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="./_css/bootstrap-datepicker3.min.css"/>
		<link rel="stylesheet" type="text/css" href="./css/style.min.css"/>
		<link rel="stylesheet" type="text/css" href="./css/flash-messages.css"/>
		
		<script type="text/javascript" src="./js/jquery.flash_messages.js"></script>
		<script type='text/javascript' src='./js/translite.js'></script>
		<script type='text/javascript' src='./js/app.routeConfig.js'></script>
		<script type='text/javascript' src='./js/directives/filter.js'></script>
		<script type='text/javascript' src='./js/directives/pager.js'></script>
		<script type='text/javascript' src='./js/app.directive.modalAddPage.ctrl.js'></script>
		<script type='text/javascript' src='./js/services/api.js'></script>
		<script type='text/javascript' src='./js/services/mainService.js'></script>
		<script type='text/javascript' src='./js/app.js'></script>
		<script type='text/javascript' src='./js/controllers/default.ctrl.js'></script>
		
		<?foreach($API->getControllers() as $controller){?>
			<script type='text/javascript' src='./js/controllers/<?=$controller?>.ctrl.js'></script>
		<?}?>
		
		<script type='text/javascript' src='./js/onload.js'></script>
		
		<script type='text/javascript'>
			var menuStructure = <?=$menuStructure?>; 
			var systemInfo = <?=$systemInfo?>; 
			var availableModules = <?=$availableModules?>;
			var availableControllers = <?=$availableControllers?>;
			var availableTypes = <?=$availableTypes?>;
			var typesInterconnection = <?=$typesInterconnection?>;
			var typesEditAccess = <?=$typesEditAccess?>;
			var typesAddAccess = <?=$typesAddAccess?>;
			var typesDelAccess = <?=$typesDelAccess?>;
		</script>
		
	</head>


	<body>
		
		<nav class="navbar navbar-fixed-top" role="navigation" ng-controller='topMenuController as topMenuCtrl'>
			<div class="container">
				
				<div class="navbar-header">
					<a class="navbar-brand" href="#/"><span class='icon-pscircle'></span></a>
				</div>
				
				<div class="dropdown" ng-repeat='section in topMenuCtrl.menuStructure'>
					<button class="btn btn-link dropdown-toggle" type="button" id="dropdownMenu{{$index}}" data-toggle="dropdown" aria-expanded="true">
						<span class="{{section.icon}}"></span>
						<span class="hidden-xs">{{section.header}}</span>
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu{{$index}}">
						<li role="presentation" ng-repeat='module in section.modules'>
							<a role="menuitem" tabindex="-1" href="#{{module.alias}}">{{module.header}}</a>
						</li>
					</ul>
				</div>
				
				<div id='nav-right-container'>
					<div class='menu-section sitelink'>
						<a href='{{topMenuCtrl.siteURI}}' title='back to {{topMenuCtrl.siteName}}' target='_blank'><?=SITENAME?></a>
					</div>
					<div class='menu-section logout'>
						<a class='logo1ut' href="/?logout=1" title='exit'><span class='icon-off'></span></a>
					</div>
				</div>
				
				
			</div>
		</nav>
		
		<div class='container'>
			<div class="row">
				<ng-view></ng-view>
			</div>
		</div>
		
		<img id='preloader' src='./css/preloader.gif'>
		
	</body>

</html>
