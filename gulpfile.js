// plugins
var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var prefix = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var minify = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');


var params = {
	styles: {
		src: './scss/**/*.scss',
		dest: './css',
		concat: "style.min.css",
		prefixes: ['last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
		sass: {
			sourceComments: false,
			outputStyle: 'compressed',
			errLogToConsole: true
		}
	}
}



gulp.task('sass', function ()
{
	gulp.src(params.styles.src)
		.pipe(sourcemaps.init())
		.pipe(sass(params.styles.sass).on('error', sass.logError))
		.pipe(prefix(params.styles.prefixes))
		.pipe(minify())
		.pipe(concat(params.styles.concat))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(params.styles.dest));
});


gulp.task('default', ['sass'], function() {
	gulp.watch(params.styles.src, ['sass'])
	.on('change', function(evt) {
		console.log(
			'[watcher] File ' + evt.path.replace(/.*(?=sass)/,'') + ' was ' + evt.type + ', compiling...'
		);
	});
});
