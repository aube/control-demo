function filtersController(mainService,$location)
{
	var self = this;
	
	self.filters = [];
	
	self.setFilters = function(filters)
	{
		self.filters = filters;
	}
	
	self.getFilters = function()
	{
		return JSON.stringify($location.search());
	}
	
	
	self.onChangeFilter = function(input)
	{
		var filter = input.filter;
		var search = $location.search();
		search[filter.alias] = filter.data;
		$location.search(search);
	}
	
	self.updateFilters = function()
	{
		for (var f in self.filters)
		{
			var filter = self.filters[f];
			self.filters[f].data = $location.search()[filter.alias];
		};
	}
	
	mainService.setFilters = self.setFilters;
	mainService.updateFilters = self.updateFilters;
	mainService.getFilters = self.getFilters;
	
}
