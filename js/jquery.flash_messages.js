(function($) {
	$.fn.flash_message = function(options) {
		
		options = $.extend({
			text: 'Done',
			time: 1000,
			how: 'first', //first|last
			class_name: ''
		}, options);
		
		return $(this).each(function() {
			
			options.text = (options.text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br>' + '$2');
			
			var message = $('<div />', {
				'class': 'flash-message '+options.class_name
				
			}).html(options.text).hide().fadeIn('fast');
			
			if (options.how == 'first')
				$(this).prepend(message);
			else
				$(this).append(message);
			
			if (options.time > 0)
			{
				message.delay(options.time).fadeOut('normal', function() {
					$(this).remove();
				});
			}
			else
			{
				message.click(
					function(){
						message.fadeOut(
							'normal',
							function() {
								$(this).remove();
							}
						)
					}
				);
			}
		
		});
	};
})(jQuery);

jQuery( document ).ready(function( $ ) {
	$( "body" ).append($('<div />', {
			id: 'flash-message-area'
		}));
	setInterval(flashMessages.show, flashMessages.updateInterval);
});




var flashMessages = {
	time: 1000,
	updateInterval:500,
	container:'#flash-message-area',
	data: []
};


//добавление сообщения в очередь
flashMessages.add = function(text, time, class_name)
{
	var added=false;
	$.each(flashMessages.data, function( index, value ) {
		if (value.class_name == class_name)
		{
			flashMessages.data[index].text+=text;
			flashMessages.data[index].time+=(time?time:0);
			added=true;
		}
	});
	
	if (!added)
		flashMessages.data.push({
			'text': text,
			'time': (time == undefined ? flashMessages.time : time),
			'class_name': (class_name || '')
		});
};


flashMessages.show = function(){
	if (flashMessages.data[0])
	{
		var message = flashMessages.data.splice(0,1);
		$(flashMessages.container).flash_message(message[0]);
	}
};





