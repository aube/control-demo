
function routeConfig($routeProvider)
{
	
	//load controllers
	angular.forEach(availableControllers,function(controller){
		if (typeof routes[controller] == 'function')
		{
			routes[controller]($routeProvider);
		}
	});
	
	
	//other
	$routeProvider
		.when('/default', {
			templateUrl: 'templates/default.html',
			controller: DefaultController,
			controllerAs: 'DefaultCtrl',
			reloadOnSearch: false
		})
		.otherwise({
			redirectTo: '/default'
		});
	
	//$locationProvider.html5Mode(true);
	
}




var routes = {};


routes.comments = function($routeProvider)
{
	$routeProvider
	//Comments
	.when('/comments', {
		templateUrl: 'templates/comments.html',
		controller: CommentsController,
		controllerAs: 'CommentsCtrl',
		reloadOnSearch: false
	})
	.when('/comment/:id', {
		templateUrl: 'templates/comment.html',
		controller: CommentController,
		controllerAs: 'CommentCtrl',
		reloadOnSearch: false
	});
}


routes.tree = function($routeProvider){
	$routeProvider
	//Дерево страниц
	.when('/tree', {
		templateUrl: 'templates/tree.html',
		controller: TreeController,
		controllerAs: 'TreeCtrl',
		reloadOnSearch: false
	});
}


routes.orders = function($routeProvider){
	$routeProvider
	//Заказы
	.when('/orders', {
		templateUrl: 'templates/orders.html',
		controller: OrdersController,
		controllerAs: 'OrdersCtrl',
		reloadOnSearch: false
	})
	.when('/order/:id', {
		templateUrl: 'templates/order.html',
		controller: OrderController,
		controllerAs: 'OrderCtrl',
		reloadOnSearch: false
	});
}

routes.synclogs = function($routeProvider){
	$routeProvider
	
	.when('/synclogs', {
		redirectTo: '/synclogs/queue'
	})
	.when('/synclogs/:menu', {
		templateUrl:  function(params){ return 'templates/synclogs-'+params.menu+'.html'; },
		controller: SyncLogsController,
		controllerAs: 'SyncLogsCtrl',
		reloadOnSearch: false
	});
}

routes.types = function($routeProvider){
	$routeProvider
	//Ресурсы по типам
	.when('/types', {
		templateUrl: 'templates/types.html',
		controller: TypesController,
		controllerAs: 'TypesCtrl',
		reloadOnSearch: false
	})
	.when('/types/:type', {
		templateUrl: function(params){ return 'templates/types.html'; },
		controller: TypesController,
		controllerAs: 'TypesCtrl',
		reloadOnSearch: false
	})
	.when('/type/:type/:idtype', {
		redirectTo: function(params){ return '/type/'+params.type+'/'+params.idtype+'/type'; }
	})
	.when('/type/:type/:idtype/:menu', {
		templateUrl: function(params){ return 'templates/type-'+params.menu+'.html'; },
		controller: TypeController,
		controllerAs: 'TypeCtrl',
		reloadOnSearch: false
	})
	.when('/type/:type/:idtype/:menu/:id', {
		templateUrl: 'templates/type-page-fields.html',
		controller: TypeController,
		controllerAs: 'TypeCtrl',
		reloadOnSearch: false
	});
}

routes.users = function($routeProvider){
	$routeProvider
	//Пользователи
	.when('/users', {
		templateUrl: 'templates/users.html',
		controller: UsersController,
		controllerAs: 'UsersCtrl'
	})
	.when('/user/:id', {
		templateUrl: 'templates/user.html',
		controller: UserController,
		controllerAs: 'UserCtrl'
	});
}

routes.statichtml = function($routeProvider){
	$routeProvider
	.when('/statichtml', {
		templateUrl: 'templates/statichtml.html',
		controller: StaticHTMLController,
		controllerAs: 'StaticHTMLCtrl',
		reloadOnSearch: false
	});
}

routes.statistics = function($routeProvider){
	$routeProvider
	.when('/statistics', {
		templateUrl: 'templates/statistics.html',
		controller: StatisticsController,
		controllerAs: 'StatisticsCtrl'
	});
}

routes.subscribers = function($routeProvider){
	$routeProvider
	.when('/subscribers', {
		templateUrl: 'templates/subscribers.html',
		controller: SubscribersController,
		controllerAs: 'SubscribersCtrl'
	});
}

routes.redirects = function($routeProvider){
	$routeProvider
	//Переадресация
	.when('/redirects', {
		templateUrl: 'templates/redirects.html',
		controller: RedirectsController,
		controllerAs: 'RedirectsCtrl'
	});
}

routes.requests = function($routeProvider){
	$routeProvider
	//Запросы
	.when('/requests', {
		templateUrl: 'templates/requests.html',
		controller: RequestsController,
		controllerAs: 'RequestsCtrl',
		reloadOnSearch: false
	});
}


routes.admin = function($routeProvider){
	$routeProvider
	.when('/admin', {
		redirectTo:'/admin/operations'
	})
	.when('/admin/:menu', {
		templateUrl: 'templates/admin.html',
		controller: AdminController,
		controllerAs: 'AdminCtrl'
	});
}

routes.billingbalance = function($routeProvider){
	$routeProvider
	//Биллинг
	.when('/billingbalance', {
		templateUrl: 'templates/billingbalance.html',
		controller: BillingBalanceController,
		controllerAs: 'BillingBalanceCtrl',
		reloadOnSearch: false
	});
}

routes.billinghistory = function($routeProvider){
	$routeProvider
	//Биллинг
	.when('/billinghistory', {
		templateUrl: 'templates/billinghistory.html',
		controller: BillingHistoryController,
		controllerAs: 'BillingHistoryCtrl',
		reloadOnSearch: false
	});
}

routes.yandexkassa = function($routeProvider){
	$routeProvider
	//Платежи Яндекс
	.when('/yandexkassa', {
		templateUrl: 'templates/yandexkassa.html',
		controller: YandexKassaController,
		controllerAs: 'YandexKassaCtrl',
		reloadOnSearch: false
	});
}

routes.delivery = function($routeProvider){
	$routeProvider
	//delivery
	.when('/delivery', {
		redirectTo: '/delivery/courier'
	})
	.when('/delivery/:type', {
		templateUrl:  function(params){ return 'templates/delivery-'+params.type+'.html'; },
		controller: DeliveryController,
		controllerAs: 'DeliveryCtrl',
		reloadOnSearch: false
	});
}

routes.imagesstorage = function($routeProvider){
	$routeProvider
	//Images
	.when('/imagesstorage', {
		templateUrl: 'templates/imagesstorage.html',
		controller: IStorageController,
		controllerAs: 'IStorageCtrl'
	})
	.when('/imagesstorage/:type', {
		templateUrl: 'templates/imagesstorage.html',
		controller: IStorageController,
		controllerAs: 'IStorageCtrl'
	});
}

routes.places = function($routeProvider){
	$routeProvider
	//places
	.when('/places', {
		templateUrl: 'templates/places.html',
		controller: PlacesController,
		controllerAs: 'PlacesCtrl',
		reloadOnSearch: false
	})
	.when('/places/:id', {
		templateUrl:  'templates/place.html',
		controller: PlaceController,
		controllerAs: 'PlaceCtrl',
		reloadOnSearch: false
	});
}

routes.marketxml = function($routeProvider){
	$routeProvider
	.when('/marketxml', {
		templateUrl:  'templates/marketxml.html',
		controller: MarketXMLController,
		controllerAs: 'MarketXMLCtrl',
		reloadOnSearch: false
	});
}
