
var panel = {};


panel.disconnectPage = function()
{
	if (confirm('disconnect page?'))
	{
		$.post('/control/api.php',{"action":"disconnectPage","type":"<?=$type?>","idtype":"<?=$idtype?>","parent":page.parent},
			function(d){location.reload()}).fail(function(){'delete error'});
	}
}


panel.addAndConnectPage = function(type,name,parent)
{
	$('#modal-addpage').modal('show');
	$('#modal-title').text(name);
	panel.addPageType = type;
}


panel.addPage = function()
{
	$.post('/control/api.php',{'action':'addPage','type':panel.addPageType,'alias':panel.modal_alias.val(),'header':panel.modal_header.val(),'parent':page.id}).then(function(d) {
		location.replace(location.pathname+(location.pathname=='/'?'':'/')+panel.modal_alias.val());
	});
	
};


panel.delPage = function()
{
	if (confirm("Удалить страницу?"))
		$.post('/control/api.php',{'action':'delPage','page':page}).then(function(d) {
			location.replace(page.path);
		});
	
};


panel.editModeEnabled = function(element)
{
	
	$(element).addClass('active');
	element.onclick = function(){panel.saveResource(element)};
	
	tinymce.init({
		selector: 'h1.editable',
		inline: true,
		toolbar: 'undo redo',
		menubar: false
	});

	tinymce.init({
		selector: 'div.editable',
		inline: true,
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
	});
	
	
	
	tinymce.init({
		selector: 'article.editable',
		inline: true,
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
	});
	
	
	function createImages(images)
	{
		$('#editableImagesContainer').remove();
		var imagesContainer = $( "<div id='editableImagesContainer'></div>");
		$('.editable').last().after(imagesContainer);
		
		for (var n in images)
		{
			var name = images[n].name ? images[n].name : images[n];
			$('<img src="'+page.images_src+'/'+name+'" >').appendTo(imagesContainer);
		}
		imagesContainer.after($('#uploadFilesContainer'));
	}
	createImages(page.images_list);

	
	var url = '/control/api.php?action=uploadTypeImg&idtype='+page.idtype+'&type='+page.type;
	$('#fileupload').fileupload({
		url: url,
		dataType: 'json',
		start: function (e, data) {
			$('#progress').show();
		},
		done: function (e, data) {
			createImages(data.result);
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			if (progress == 100)
			{
				$('#progress').hide();
			}
			$('#progress .progress-bar').css(
				'width',
				progress + '%'
			);
		}
	}).prop('disabled', !$.support.fileInput)
		.parent().addClass($.support.fileInput ? undefined : 'disabled');
	
	
	
}

panel.saveResource = function(element)
{
	$(element).removeClass('active');
	element.onclick = function(){panel.editModeEnabled(element)};
	
	var data = {};
	$('.editable').each(function(){
		var el = $(this).first();
		data[el.data('field')] = el.html();
	});
	data.idtype = page.idtype;
	data.type = page.type;
	data.id = page.id;
	data.parent = page.parent;
	
	$('#editableImagesContainer').remove();
	
	
	$.post('/control/api.php',{"action":"saveResource","type":page.type,"params":data})
	.then(function(d) {
		location.reload();
	})
	.fail(function(err) {
	})
	.done();
}


$('#controlPanelTrigger').bind('click',function(){$('#controlPanel').toggleClass('turn-aside')});

panel.modal_submitbutton = $('#modal-submitbutton');
panel.modal_submitbutton.bind('click',panel.addPage);
panel.modal_header = $('#modal-input-header');
panel.modal_alias = $('#modal-input-alias');
panel.modal_header.bind('keyup',function(){
	panel.modal_alias.val(translite_alias(panel.modal_header.val()));
});


