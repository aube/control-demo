function pagerController(mainService,$location)
{
	var self = this;
	
	self.limit = 20;
	self.pagesAmount = 0;
	self.rowsAmount = 0;
	
	setInterval(function(){
		self.currentPage = $location.search().page || 1;
	},200)
	
	
	self.setRowsAmount = function(amount)
	{
		self.rowsAmount = amount;
		self.pagesAmount = Math.floor(self.rowsAmount / self.limit)+1;
	}
	mainService.setRowsAmount = self.setRowsAmount;
	
	self.nextPage = function()
	{
		self.currentPage++;
		if (self.currentPage>self.pagesAmount)
			self.currentPage=self.pagesAmount;
		else
			self.onChangePage();
	}
	
	self.prevPage = function()
	{
		self.currentPage--;
		if (self.currentPage==0)
			self.currentPage=1;
		else
			self.onChangePage();
	}
	
	self.onChangePage = function()
	{
		
		self.currentPage = parseInt(self.currentPage)||1;
		if (self.currentPage==0)
			self.currentPage=1;
		if (self.currentPage>self.pagesAmount)
			self.currentPage=self.pagesAmount;
		
		//update params string
		var search = $location.search();
		search.page = self.currentPage;
		$location.search(search);
		
	}
	
}
