
function StaticHTMLController(mainService,api,$location)
{
	var self = this;
	
	self.data = {};
	
	self.filters = [
		{'header':'id','alias':'id','data':null},
		{'header':'name','alias':'name','data':null},
		{'header':'path','alias':'path','data':null},
		{'header':'header','alias':'header','data':null}
	];
	self.newPage = {
		'action':'addPage',
		'name':'',
		'header':'',
		'type':'doc'
	};
	
	
	self.changeCurrentFile = function(file)
	{
		self.currentFile = file;
		$location.search({'file':self.currentFile.name});
	}
	
	self.header = mainService.getActiveSectionHeader('statichtml');
	
	
	//get data
	self.dataExists = false;
	self.getData = function(){
		
		if (self.dataExists == true)
			return;
		
		var filters = JSON.stringify($location.search());
		
		api.post({'action':'getStaticHTMLFiles'}).then(function(d) {
			self.files = d;
			if (!self.currentFile)
				self.currentFile = self.files[0];
		});
		
		self.dataExists = true;
		
	}
	mainService.getData = self.getData;
	
	
	//save file
	self.saveStaticHTMLFile = function()
	{
		
		api.post({'action':'saveStaticHTMLFile','file':self.currentFile.name,'content':self.currentFile.content}).then(function(d) {
			self.dataExists = false;
		});
	}
}
