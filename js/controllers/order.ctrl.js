
function OrderController(mainService,api,$route)
{
	var self = this;
	
	self.id = $route.current.params.id;
	
	self.header = mainService.getActiveSectionHeader('orders');
	
	//get data
	self.data = {};
	self.dataExists = false;
	self.getData = function(){
		
		if (self.dataExists===true)
			return;
		
		api.post({'action':'getOrder','id':self.id}).then(function(d) {
			self.data = d;
		});
		self.dataExists = true;
		
	}
	mainService.getData = self.getData;
	
	
	//prepay
	self.setPrepay = function()
	{
		api.post({'action':'setOrderPrepay','id':self.id}).then(function(d) {
			self.dataExists = false;
		});
	}
	self.clearPrepay = function()
	{
		api.post({'action':'clearOrderPrepay','id':self.id}).then(function(d) {
			self.dataExists = false;
		});
	}
	self.testSetPrepay = function()
	{
		if (self.data.prepay>0) return false;
		if (self.data.account_balance<self.data.total) return false;
		return true;
	}
	self.testClearPrepay = function()
	{
		if (self.data.prepay>0) return true;
	}
}
