
function DeliveryController($route,mainService,api,$location)
{
	var self = this;
	
	self.header = mainService.getActiveSectionHeader('delivery');
	
	self.data = {};
	self.type = $route.current.params.type;
	
	
	self.menus = [
			{'name':'courier','header':'Курьерская доставка'},
			{'name':'pickup','header':'Точки самовывоза'}
		];
	
	self.currentFilters = false;
	
	
	//get data
	
	self.getData = function(){
		
		var filters = JSON.stringify($location.search());
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			
			var action = self.type == 'courier'?'getDeliveryCourier':'getDeliveryPickup';
			
			self.currentFilters = filters;
			api.post({'action':action,'order':'dt-','filter':filters}).then(function(d) {
				self.data = d;
				self.rows = d.rows;
				
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				//pager
				mainService.setRowsAmount(d.found_rows);
				
			});
		}
	}
	mainService.getData = self.getData;
	
	
	
	self.delivery = {};
	
	self.showDeliveryWindow = function(id)
	{
		
		self.delivery = (id != undefined ? self.rows[id] : {name:'',city:''});
		self.delivery.prices = [];
		self.delivery.type = self.type;
		
		if (self.type == 'courier')
		{
			self.delivery.name = self.delivery.name=='' ? 'Курьерская доставка' : self.delivery.name;
			$('#modal-delivery-courier').modal('show');
		}
		else
		{
			$('#modal-delivery-pickup').modal('show');
		}
		
		self.getDeliveryPrices(self.delivery.id);
		
	};
	
	
	
	self.addDeliveryPrice = function()
	{
		self.delivery.prices.push({'border':0,'price':0});
	}
	self.delDeliveryPrice = function(index)
	{
		self.delivery.prices.splice(index,1);
	}
	
	self.getDeliveryPrices = function(id)
	{
		api.post({'action':'getDeliveryPrices','devtype':self.type,'devid':id}).then(function(d) {
			self.delivery.prices = d;
		});
	};
	
	
	
	self.updatePickupPoints = function()
	{
		api.post({'action':'updatePickupPoints'}).then(function(d) {
			self.currentFilters = false;
		});
	};
	
	
	
	self.delDelivery = function()
	{
		api.post({'action':'delDelivery', 'devid':self.delivery.id, 'devtype':self.type}).then(function(d) {
			self.currentFilters = false;
			if (self.type == 'courier')
				$('#modal-delivery-courier').modal('hide');
			else
				$('#modal-delivery-pickup').modal('hide');
		});
	};
	
	
	self.saveDelivery = function()
	{
		var options = $.extend({
			action: (self.delivery.id ? 'editDelivery' : 'addDelivery'),
			type: self.type
		}, self.delivery);
		
		
		api.post(options).then(function(d) {
			self.currentFilters = false;
			if (self.type == 'courier')
				$('#modal-delivery-courier').modal('hide');
			else
				$('#modal-delivery-pickup').modal('hide');
		});
	};
	
	
}
