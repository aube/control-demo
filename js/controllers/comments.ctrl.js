
function CommentsController(mainService,api,$location)
{
	var self = this;
	
	self.data = {};
	
	
	self.rowClick = function(id){
		location.hash = '/comment/'+id;
	};
	
	self.currentFilters=false;
	
	
	//get data
	
	self.getData = function(){
		
		var filters = JSON.stringify($location.search());
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			self.currentFilters = filters;
			api.post({'action':'getComments','filter':filters}).then(function(d) {
				self.data = d;
				self.rows = d.rows;
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				//pager
				mainService.setRowsAmount(d.found_rows);
				
			});
		}
	}
	mainService.getData = self.getData;
	
	
}
