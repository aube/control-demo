
function BillingBalanceController(mainService,api,$location)
{
	var self = this;
	
	self.data = {};
	
	self.header = mainService.getActiveSectionHeader('billingbalance');
	
	self.getOperations = function(account, currentAccountOperations)
	{
		self.currentAccountOperations = currentAccountOperations;
		api.post({'action':'getOperations','account':account}).then(function(d) {
			self.operations = d;
			$('#modal-operations').modal('show');
		});
		
	};
	
	//get data
	self.currentFilters = false;
	self.getData = function(){
		
		var filters = JSON.stringify($location.search());
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			self.currentFilters = filters;
			api.post({'action':'getBalance','order':'summ-','filter':filters}).then(function(d) {
				self.data = d;
				self.rows = d.rows;
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				//pager
				mainService.setRowsAmount(d.found_rows);
				
			});
		}
	}
	mainService.getData = self.getData;
	
}
