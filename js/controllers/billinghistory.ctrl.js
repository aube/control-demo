
function BillingHistoryController(mainService,api,$location)
{
	var self = this;
	
	self.data = {};
	self.newOperation = {
		summ:'',
		email:'',
		descr:'Заказ #'
		};
	
	self.header = mainService.getActiveSectionHeader('billinghistory');
	
	
	self.addOperationShow = function()
	{
		self.addOperationStatus = 1;
	};
	self.addOperationHide = function()
	{
		self.addOperationStatus = 0;
	};
	
	self.addOperation = function()
	{
		
		api.post({'action':'addOperation', 'email':self.newOperation.email, 'summ':self.newOperation.summ, 'descr':self.newOperation.descr}).then(function(d) {
			
			if (!d) return; //error
			
			self.currentFilters = false;
			self.getData();
			self.newOperation = {
				summ:'',
				email:'',
				descr:'Заказ #'
				};
		});
		
	};
	
	
	
	//get data
	self.currentFilters = false;
	self.getData = function(){
		
		var filters = JSON.stringify($location.search());
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			self.currentFilters = filters;
			api.post({'action':'getHistoryPayments','order':'dt-','filter':filters}).then(function(d) {
				self.data = d;
				self.rows = d.rows;
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				//self.filtersUpdate();
				
				//pager
				mainService.setRowsAmount(d.found_rows);
				
			});
		}
	}
	mainService.getData = self.getData;
	
}
