
function RequestsController(mainService,api,$location)
{
	var self = this;
	
	self.data = {};
	self.currentFilters = false;
	self.filters = [
		{'header':'Номер запроса','alias':'id','data':null},
		{'header':'Дата запроса','alias':'dt','data':null},
		{'header':'Имя','alias':'name','data':null},
		{'header':'Контакт','alias':'contact','data':null},
		{'header':'Содержимое','alias':'message','data':null}
	];
	
	self.header = mainService.getActiveSectionHeader('requests');
	
	self.onChangeFilter = mainService.onChangeFilter;
	
	
	self.filtersUpdate = function()
	{
		for (var f in self.filters)
		{
			var filter = self.filters[f];
			self.filters[f].data = $location.search()[filter.alias];
		};
	}
	
	self.rowClick = function(id)
	{
		self.request = self.rows[id];
		$('#modal-request').modal('show');
	};
	
	self.saveRequest = function(oldValue)
	{
		if (oldValue != self.request.answer)
		{
			api.post({'action':'editRequest', 'id':self.request.id, 'answer':self.request.answer}).then(function(d) {
				//console.log(d);
			});
		}
	}
	
	
	
	//get data
	
	self.getData = function(){
		
		var filters = JSON.stringify($location.search());
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			self.currentFilters = filters;
			api.post({'action':'getRequests','order':'dt-','filter':filters}).then(function(d) {
				self.data = d;
				self.rows = d.rows;
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				//self.filtersUpdate();
				
				//pager
				mainService.setRowsAmount(d.found_rows);
				
			});
		}
	}
	mainService.getData = self.getData;
	
	
}
