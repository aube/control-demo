
function AdminController(mainService, api, $route)
{
	var self = this;
	
	self.data = {};
	
	
	self.menu = $route.current.params.menu;

	self.menus = [
		{'header':'Операции','name':'operations'}
		,{'header':'php.ini','name':'php.ini'}
		,{'header':'errors','name':'errors'}
	];
	
	self.operations = [
		{'header':'Восстановление сортировки изображений','name':'imagesSortAllTestAndRestore'}
		,{'header':'Восстановление путей','name':'restorePathsByParent'}
		,{'header':'Пересчет поля childs','name':'restoreChildsByParent'}
	];
	
	self.results = [];
	
	self.copy = function(){
		//copyPages
		api.post({'action':'copyPages', from:self.copy_from, to:self.copy_to}).then(function(d) {
			self.results = d;
			
		});
	};
	
	self.deletePages = function(){
		//deletePages
		api.post({'action':'deletePages', 'delete_path':self.delete_path}).then(function(d) {
			self.results = d;
			
		});
	};
	
	
	self.copy_from = '';
	self.copy_from = '';
	self.delete_path = '';
	
	self.ini = [];
	self.config = [];
	self.iniFilter = '';
	
	if (self.menu == 'php.ini')
	api.post({'action':'getPhpini'}).then(function(d) {
		angular.forEach(d,function(v,k){
			v.key = k;
			self.ini.push(v);
			})
	});
	
	
	if (self.menu == 'errors')
	api.post({'action':'getErrorLog'}).then(function(d) {
		self.errors = d;
	});
	
	
	
	self.header = mainService.getActiveSectionHeader('admin');
	
	//action motor!
	self.motor = function(action)
	{
		api.post({'action':action}).then(function(d) {
			self.results = d.result_txt;
			
		});
	}
}
