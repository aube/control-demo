function TypeController($window,mainService,api,FileUploader,$location,$route)
{

	var self = this;
	self.idtype = $route.current.params.idtype;
	self.type = $route.current.params.type;
	self.menu = $route.current.params.menu;
	self.id = $route.current.params.id;
	
	
	
	self.filters = $location.search();
	//~ self.onChangeFilter = function(input)
	//~ {
		//~ var filters = {};
		//~ angular.forEach(self.filters, function(value, key){
			//~ if (value > '')
				//~ filters[key] = value;
		//~ });
		//~ $location.search(filters);
	//~ }
	
	self.checkModuleAccess = function(module)
	{
		return mainService.checkModuleAccess(module);
	}

	////////////////
	//tinymce
	
	self.getTinyMCEImageList = function()
	{
		var image_list = [];
		
		angular.forEach(self.images, function(value, key) {
			image_list.push({title: value.name, value: value.src+value.name});
		});
		
		return image_list;
	}
	
	self.tinyMCEData = '';
	self.activeImage = {};
	self.activeImageKey = '';
	self.tinymceOptions = {
		menubar: false,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | link | code fullscreen | removeformat",
		relative_urls: true,
		image_list: self.getTinyMCEImageList,
		height: $window.innerHeight/3,
		handle_event_callback: function (e) {
			console.log(e);
		}
	};
	
	
	self.tinyMCEShow = function(field)
	{
		self.tinyMCEfield = field;
		self.tinyMCEData = self.resource[field];
		
		$('#modal-textareaEdit').modal('show');
	}

	
	self.tinyMCEComplete = function()
	{
		
		//после размещений изображения перетаскиванием не обновляются данные модели, поэтому:
		var content = tinyMCE.get('tinyId').getBody().innerHTML;
		content = content.replace(/\sng-src="[^]*?"/g, "");
		content = content.replace(/\sdata-mce-[^]*?"[^]*?"/g, "");
		content = content.replace(new RegExp('ng-repeat="image in TypeCtrl.images"', 'g'),'');
		content = content.replace(new RegExp('class="thumbnail ng-scope"', 'g'),'');
		content = content.replace(new RegExp(' style="undefined"', 'g'),'');
		content = content.replace(new RegExp('type="mce-text/javascript"', 'g'),'type="text/javascript"');
		content = content.replace(new RegExp(location.origin.toString(), 'g'),'');
		self.tinyMCEData = content;
		
		self.resource[self.tinyMCEfield] = self.tinyMCEData;
		
		$('#modal-textareaEdit').modal('hide');
		
	}
	//tinymce
	////////////////
	
	
	
	
	self.fields = {};
	
	self.fields.pages = [
		
		{'alias':'header',		'name':'Заголовок (Н1)','type':'input',		'placeholder':'Заголовок страницы'},
		{'alias':'menu',		'name':'Меню',			'type':'input',		'placeholder':'Краткое наименование для меню'},
		{'alias':'breadcramb',	'name':'Хрошки',		'type':'input',		'placeholder':'Хлебные крошки'},
		{'alias':'title',		'name':'title',			'type':'input',		'placeholder':'Заголовок окна браузера'},
		{'alias':'keywords',	'name':'keywords',		'type':'textarea',	'placeholder':'Ключевые слова'},
		{'alias':'description',	'name':'description',	'type':'textarea',	'placeholder':'Описание'},
	];
	//для удобства сравнения игнорируемыe полей
	self.fields.typeIgnoredFields = ["idtype","idu","author","dt","dt_edit","dt_public","breadcrambs","photo","include","tpl","rated","commented","moderated"];
	for (var t in self.fields.pages)
	{
		self.fields.typeIgnoredFields.push(self.fields.pages[t].alias);
	}
	
	self.commentedValues = {
		'no':'Запретить всем',
		'yes':'Только зарегистрированным',
		'all':'Всем посетителям',
	}
	self.moderatedValues = {
		'no':'Публиковать без проверки',
		'yes':'Проверять все комментарии',
		'all':'Проверять комментарии гостей',
	}
	self.commentRowClick = function(id){
		location.hash = "/comment/"+id;
	}
	
	
	////////////////
	//add page
	
	self.newPage = {
		'action':'addPage',
		'name':'',
		'header':'',
		'type':'doc'
	};
	
	self.addPageWindowShow = function()
	{
		self.newPage.parent = self.id;
		mainService.modalCtrl = self;
		$('#modal-addpage').modal('show');
		
	};
	
	self.addPage = function()
	{
		$('#modal-addpage').modal('hide');
		self.newPage.name = translite3(self.newPage.header);
		api.post(self.newPage).then(function(d) {
			self.currentPage = false;
			self.newPage.name = '';
			self.newPage.header = '';
		});
	};
	
	self.delPage = function()
	{
		if (confirm("Удалить страницу?"))
			api.post({'action':'delPage','page':self.page}).then(function(d) {
				location.hash = '/type/'+self.type+'/'+self.idtype+'/resource';
			});
	};
	
	self.delResource = function()
	{
		if (confirm("Удалить ресурс?"))
			api.post({'action':'delResource','type':self.type,'idtype':self.idtype}).then(function(d) {
				location.hash = '/types/'+self.type;
			});
	};
	self.pageMaybeDel = false;
	
	//add page
	////////////////
	
	
	
	////////////////
	//get data
	self.resource = {};
	self.resourceChanged = false;
	self.currentPage = false;
	
	self.getData = function()
	{
		
		var filters = JSON.stringify($location.search());
		
		if (!self.type)
			return;
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			self.currentFilters = filters;
			
			api.post({'action':'getResourse','type':self.type,'idtype':self.idtype,'filter':filters}).then(function(d)
			{
				self.resource = d.resource;
				self.path = self.resource.id>0 ? (self.resource.path+'/'+self.resource.alias) : "";
				self.path = self.path.replace('//','/');
				self.typeName = d.typeName;
				self.pages = d.pages;
				self.parents = d.parents;
				self.images = d.images;
				self.upload_max_filesize = d.upload_max_filesize;
				self.comments = d.comments;
				self.comments_found_rows = d.comments_found_rows;
				
				if ('comments' == self.menu)
				{
					mainService.setRowsAmount(self.comments_found_rows);
				}
				
				//возможность удаления страницы
				self.pageMaybeDel = !d.resource.id;
				
				if ('images' == self.menu)
				{
					
					self.imagesParams = d.images_params;
					self.sortableImagesUpdate();
				}
				
				
				
				//подготовка списка полей для вкладки редактирования типа
				if ('resource' == self.menu)
				{
					self.fields.type=[];
					for (var alias in d.type_fields)
					{
						if (self.fields.typeIgnoredFields.indexOf(alias)>=0)
							continue;
						
						var type_fields = d.type_fields[alias].split(',');
						var field = {};
						field.alias = alias;
						
						//формирование полей по данным из бэкэнда
						if (type_fields[0]=='textarea' || type_fields[0]=='input') //прописанные в классе значения
						{
							field.name = type_fields[1];
							field.type = type_fields[0]=='textarea'?'textarea':'input';
							field.placeholder = type_fields[1];
						}
						else //автоформирование полей
						{
							field.name = alias;
							field.type = d.type_fields[alias] == 'text'?'textarea':'input';
						}
						self.fields.type.push(field);
					}
					
					//заполнение данных полей
					for (var field in self.fields.type)
					{
						var alias = self.fields.type[field].alias;
						self.fields.type[field].data = self.resource[alias];
					}
					
					//активация календаря
					$('.datepicker').datepicker({
						format: "yyyy-mm-dd 00:00:00",
						weekStart: 1,
						todayBtn: "linked",
						clearBtn: true,
						language: "ru",
						orientation: "top auto",
						autoclose: true,
						todayHighlight: true
					});
					
				}
				
				
				if ('page' == self.menu)
				{
					
					angular.forEach(self.pages, function(page){
							if (self.id==page.id)
								self.page = page;
						});
					
					//заполнение данных полей
					for (var field in self.fields.common)
					{
						var alias = self.fields.common[field].alias;
						self.fields.common[field].data = self.resource[alias];
					}
					
					//активация календаря
					$('.datepicker').datepicker({
						format: "yyyy-mm-dd 00:00:00",
						defaultDate: self.page.dt_public,
						weekStart: 1,
						todayBtn: "linked",
						clearBtn: true,
						language: "ru",
						orientation: "top auto",
						autoclose: false,
						todayHighlight: true
					});
				}
			});
			
		}
	}
	
	mainService.getData = self.getData;
	//get data
	////////////////



	////////////////
	//edit page
	
	//функция подбора связанных ресурсов
	self.linked = {};
	self.linked.alias;
	self.linked.field;
	self.linked.current = [];
	self.linked.modalTitle = "";
	self.linked.type = "item";
	self.linked.searchResult = [];
	
	self.linked.activate = function(field)
	{
		self.linked.alias = field.alias;
		self.linked.field = self.resource[field.alias];
		
		//set type
		var type = field.alias.replace('linked_','');
		type = type.substr(0,type.length-1);
		self.linked.type = type;
		self.linked.modalTitle = mainService.getTypenameByClassname(type);
		
		if (self.linked.modalTitle > '')
		{
			$('#modal-linked').modal('show');
			
			if (self.linked.field > '')
			api.post({'action':'getLinked','current':self.linked.field,'type':self.linked.type})
				.then(function(result){
					self.linked.current = result;
				});
		}
	}
	//поиск
	self.linked.search = function(text, varArray)
	{
		if (self.linked.searchString > '')
		api.post({'action':'getLinked','search':self.linked.searchString,'type':self.linked.type})
			.then(function(result){
				self.linked.searchResult = result;
			});
	};
	//удаление
	self.linked.drop = function(index)
	{
		self.linked.searchResult.push(self.linked.current[index]);
		self.linked.current.splice(index,1);
		var ids = [];
		angular.forEach(self.linked.current, function(value, key) {
			ids.push(value.idtype);
		});
		self.resource[self.linked.alias] = ids.join();
	};
	
	//добавление
	self.linked.add = function(index)
	{
		self.linked.current.push(self.linked.searchResult[index]);
		self.linked.searchResult.splice(index,1);
		var ids = [];
		angular.forEach(self.linked.current, function(value, key) {
			ids.push(value.idtype);
		});
		self.resource[self.linked.alias]  = ids.join();
		clog(self.linked.field);
	};
	
	
	
	//edit page
	////////////////




	////////////////
	//save page
	self.saveResource = function()
	{
		
		api.post({'action':'saveResource','type':self.type,'params':self.resource});
		
		
	}
	self.savePage = function()
	{
		
		api.post({'action':'savePage','id':self.id,'params':self.page});
		
		
	}
	
	//save page
	////////////////



	////////////////
	//images
	self.sortableImagesUpdate = function()
	{
		//return;
		//сортировка
		$( "#sortableImages" ).sortable({
			handle:'.thumbnail'
			,start: function(event, ui){
				event.stopPropagation();
			}
			,update: function(event, ui){
				self.imageSort(ui.item[0]);
			}
		});
		$( "#sortableImages" ).disableSelection();
		
	};
	self.showUploadProgress = function()
	{
		return !!($window.File && $window.FormData);
	};
	self.imageEditWindowShow = function(id)
	{
		$.each(self.images, function( index, value ) {
			if (id == value.id)
				self.activeImage = self.images[index];
		});
		$('#modal-imageEdit').modal('show');
	};
	self.imageEditWindowClose = function(key)
	{
		$('#modal-imageEdit').modal('hide');
	};
	
	
	
	self.imageSort = function(el)
	{
		var id = $(el).data('id');
		var n = 0;
		var newPosition=false;
		$.each($('#sortableImages .col-sm-3'),function(){
			if (id == $(this).data('id'))
				newPosition = n;
			n++;
		});
		
		if (newPosition!==false)
		{
			var params = { 'action':'imageSort', 'id':id, 'newPosition':newPosition+1, 'type':self.resource.type, 'idtype':self.resource.idtype };
			
			api.post(params).then(
				function(images)
				{
					self.images = images;
				}
			);
		}
	};
	
	
	self.imageSave = function()
	{
		var key;
		$.each(self.images, function( index, value ) {
			if (self.activeImage.id == value.id)
				key=index;
		});
		
		self.images[key].alt = self.activeImage.alt || '';
		
		var params = { 'action':'imageSave', 'type':self.resource.type, 'idtype':self.resource.idtype };
		$.extend(params,self.images[key]);
		
		api.post(params);
		
	};
	
	
	self.imageSetMain = function()
	{
		
		var params = { 'action':'imageSetMain', 'photo':self.activeImage.name, 'type':self.resource.type , 'idtype':self.resource.idtype };
		
		api.post(params).then(
			function()
			{
				$('#modal-imageEdit').modal('hide');
				self.resource.photo = self.activeImage.name;
			}
		);
		
	}
	
	
	self.imageRemove = function()
	{
		api.post({ 'action':'imageRemove', 'id':self.activeImage.id, 'type':self.resource.type, 'idtype':self.resource.idtype }).then(
			function(images)
			{
				$('#modal-imageEdit').modal('hide');
				self.images = images;
			}
		);
	};
	
	//images
	////////////////



	//UPLOADER!!!
	
	self.uploader = new FileUploader({
		url: 'api.php?action=uploadTypeImg&idtype='+self.idtype+'&type='+self.type,
		alias: 'imagesupload',
		autoUpload: true
	});

	// FILTERS

	self.uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});

	// CALLBACKS

	self.uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
		//console.info('onWhenAddingFileFailed', item, filter, options);
	};
	self.uploader.onAfterAddingFile = function(fileItem) {
		//console.info('onAfterAddingFile', fileItem);
	};
	self.uploader.onAfterAddingAll = function(addedFileItems) {
		//console.info('onAfterAddingAll', addedFileItems);
	};
	self.uploader.onBeforeUploadItem = function(item) {
		//console.info('onBeforeUploadItem', item);
	};
	self.uploader.onProgressItem = function(fileItem, progress) {
		//console.info('onProgressItem', fileItem, progress);
		//console.info(progress);
	};
	self.uploader.onProgressAll = function(progress) {
		//console.info('onProgressAll', progress);
	};
	self.uploader.onSuccessItem = function(fileItem, response, status, headers) {
		//console.info('onSuccessItem', fileItem, response, status, headers);
		//console.info('onSuccessItem-response', response);
		self.images = response;
		//console.log(response);
		
	};
	self.uploader.onErrorItem = function(fileItem, response, status, headers) {
		console.info('onErrorItem', fileItem, response, status, headers);
		flashMessages.add('<p>'+response[0]+'</p>',0,'error');
	};
	self.uploader.onCancelItem = function(fileItem, response, status, headers) {
		//console.info('onCancelItem', fileItem, response, status, headers);
	};
	self.uploader.onCompleteItem = function(fileItem, response, status, headers) {
		//console.info('onCompleteItem', fileItem, response, status, headers);
	};
	self.uploader.onCompleteAll = function() {
		//console.info('onCompleteAll');
		self.uploader.clearQueue();
		self.sortableImagesUpdate();
		//$scope.$apply();
		//console.log(self.images);
	};

}
