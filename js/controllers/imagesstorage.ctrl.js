function IStorageController($window,mainService,api,FileUploader,$location,$route)
{
	var self = this;
	
	self.images = [];
	
	
	self.currentImage = {};
	
	self.typeParams = {
		name:'',
		header:'',
		width:100,
		height:100
	};
	
	
	self.type = $route.current.params.type;
	
	self.types = [];
	api.post({'action':'getIStorageTypes'}).then(function(d) {
		self.setTypes(d);
	});
	
	
	self.setTypes = function(types)
	{
		self.types = types;
		angular.forEach(self.types, function(value){
			if (value.name == self.type)
			{
				self.typeParams = value;
			}
		});
	}
	
	
	self.showUploadProgress = function()
	{
		return !!($window.File && $window.FormData);
	};
	
	
	//добавить тип
	self.addTypeShowForm = function()
	{
		$('#modal-imageTypeAdd').modal('show');
	}
	self.addType = function()
	{
		self.typeParams.action = self.type?'editIStorageType':'addIStorageType';
		api.post(self.typeParams).then(function(d){
			$('#modal-imageTypeAdd').modal('hide');
			self.setTypes(d);
		});
	}
	
	
	//отредактировать баннер
	self.imageEditShowForm = function(image)
	{
		angular.copy(image, self.currentImage);
		$('#modal-imageEdit').modal('show');
	}
	self.editImage = function()
	{
		self.currentImage.action = 'editIStorageImage';
		self.currentImage.type = self.type;
		api.post(self.currentImage).then(function(d){
			self.dataExists = false;
			$('#modal-imageEdit').modal('hide');
		});
	}
	self.delImage = function()
	{
		self.currentImage.action = 'delIStorageImage';
		self.currentImage.type = self.type;
		api.post(self.currentImage).then(function(d){
			self.dataExists = false;
			$('#modal-imageEdit').modal('hide');
		});
	}
	
	
	
	
	//get data
	self.data = {};
	self.dataExists = false;
	
	self.getData = function()
	{
		
		if (self.dataExists !== false)
		{
			return;
		}
		
		self.dataExists = true;
		
		api.post({'action':'getIStorageList','type':self.type}).then(function(d) {
			self.images = d.images;
			self.upload_max_filesize = d.upload_max_filesize;
			//if (!self.currentType)
				//self.changeCurrentType(self.types[0].name);
		});
		
	}
	mainService.getData = self.getData;
	
	
	
	//UPLOADER!!!
	
	self.uploader = new FileUploader({
		url: 'api.php?action=uploadImageToStorage&type='+self.type,//+editService.getId(),
		alias: 'imagesStorageUpload',
		autoUpload: true
	});

	// FILTERS

	self.uploader.filters.push({
		name: 'imageFilter',
		fn: function(item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});

	// CALLBACKS

	self.uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
		//console.info('onWhenAddingFileFailed', item, filter, options);
	};
	self.uploader.onAfterAddingFile = function(fileItem) {
		//console.info('onAfterAddingFile', fileItem);
	};
	self.uploader.onAfterAddingAll = function(addedFileItems) {
		//console.info('onAfterAddingAll', addedFileItems);
	};
	self.uploader.onBeforeUploadItem = function(item) {
		//console.info('onBeforeUploadItem', item);
	};
	self.uploader.onProgressItem = function(fileItem, progress) {
		console.info('onProgressItem', fileItem, progress);
		console.info(progress);
	};
	self.uploader.onProgressAll = function(progress) {
		//console.info('onProgressAll', progress);
	};
	self.uploader.onSuccessItem = function(fileItem, response, status, headers) {
		console.info('onSuccessItem', fileItem, response, status, headers);
		console.info('onSuccessItem-response', response);
		self.dataExists = false;
	};
	self.uploader.onErrorItem = function(fileItem, response, status, headers) {
		console.info('onErrorItem', fileItem, response, status, headers);
		flashMessages.add('<p>'+response[0]+'</p>',0,'error');
	};
	self.uploader.onCancelItem = function(fileItem, response, status, headers) {
		//console.info('onCancelItem', fileItem, response, status, headers);
	};
	self.uploader.onCompleteItem = function(fileItem, response, status, headers) {
		//console.info('onCompleteItem', fileItem, response, status, headers);
	};
	self.uploader.onCompleteAll = function() {
		//console.info('onCompleteAll');
		self.uploader.clearQueue();
	};

}
