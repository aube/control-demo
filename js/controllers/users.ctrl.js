
function UsersController(mainService,api,$location)
{
	var self = this;
	
	self.data = {};
	self.currentFilters = false;
	self.filters = [
		{'header':'id','alias':'id','data':null},
		{'header':'Пользователь','alias':'name','data':null},
		{'header':'Телефон','alias':'phone','data':null},
		{'header':'Email','alias':'mail','data':null}
	];
	
	self.header = mainService.getActiveSectionHeader('users');
	
	self.onChangeFilter = mainService.onChangeFilter;
	
	self.filtersUpdate = function()
	{
		for (var f in self.filters)
		{
			var filter = self.filters[f];
			self.filters[f].data = $location.search()[filter.alias];
		};
	}
	
	self.rowClick = function(id){
		location.hash = '/user/'+id;
	};
	
	
	
	//get data
	
	self.getData = function(){
		
		var filters = JSON.stringify($location.search());
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			self.currentFilters = filters;
			api.post({'action':'getUsers','order':'id','filter':filters}).then(function(d) {
				self.data = d;
				self.rows = d.rows;
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				self.filtersUpdate();
				
				//pager
				mainService.setRowsAmount(d.found_rows);
				
			});
		}
	}
	mainService.getData = self.getData;
	
	
}
