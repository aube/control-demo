
function PlacesController(mainService, api,$location)
{
	var self = this;
	
	self.data = {};
	
	
	self.rowClick = function(id){
		location.hash = '/places/'+id;
	};
	
	
	//get data

	self.currentFilters = false;
	self.getData = function()
	{
		var filters = JSON.stringify($location.search());
		var params = {'action':'getPlaces','order':'city','filter':filters};
		
		if (self.currentFilters===false || self.currentFilters != filters)
		{
			self.currentFilters = filters;
			api.post(params).then(function(d) {
				
				self.data = d;
				self.rows = d.rows;
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				//pager
				mainService.setRowsAmount(d.found_rows);
				
			});
		}
	}
	mainService.getData = self.getData;

}
