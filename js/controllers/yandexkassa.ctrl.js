
function YandexKassaController(mainService,api,$location)
{
	var self = this;
	
	self.data = {};
	self.currentFilters = false;
	self.filters = [
		{'header':'Номер заказа/аккаунта','alias':'yk.id','data':null},
		{'header':'Дата операции','alias':'dt','data':null},
		{'header':'Тип','alias':'type','data':null},
		{'header':'Статус','alias':'status','data':null},
		{'header':'Пользователь','alias':'user_mail','data':null},
		{'header':'Сумма','alias':'summ','data':null},
		{'header':'Сумма к получению','alias':'summ_received','data':null}
	];
	
	self.header = mainService.getActiveSectionHeader('yandexkassa');
	
	self.onChangeFilter = function(input)
	{
		var filter = input.filter;
		var search = $location.search();
		search[filter.alias] = filter.data;
		$location.search(search);
	}
	
	
	self.filtersUpdate = function()
	{
		for (var f in self.filters)
		{
			var filter = self.filters[f];
			self.filters[f].data = $location.search()[filter.alias];
		};
	}
	
	self.paymentWindowShow = function(index)
	{
		self.currentRow = self.rows[index];
		$('#modal-payment').modal('show');
	};
	
	
	self.returnMoney = function()
	{
		if (confirm('Подтвердите возврат'))
		{
			var id = self.currentRow.id;
			api.post({'action':'returnYKPayment','id':id}).then(function(d) {
				self.currentFilters=false;
				$('#modal-payment').modal('hide');
			});
		}
	}
	
	
	//get data
	
	self.getData = function(){
		
		var filters = JSON.stringify($location.search());
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			self.currentFilters = filters;
			api.post({'action':'getYKPayments','order':'dt-','filter':filters}).then(function(d) {
				self.data = d;
				self.rows = d.rows;
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				//self.filtersUpdate();
				
				//pager
				mainService.setRowsAmount(d.found_rows);
				
			});
		}
	}
	mainService.getData = self.getData;
	
}
