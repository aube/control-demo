
function DefaultController(mainService, api)
{
	var self = this;
	self.dataRenew = 15;
	self.data = {};
	
	
	self.widgets = {
		orders:{
			title:'Заказы'
			,bgColor:'bg-DarkOrange'
			,data:'no data'
			,icon:'icon-shoppingcartalt'
			,visible:true
			,href:'#/orders'
			}
		,comments:{
			title:'Комментарии'
			,bgColor:'bg-RoyalBlue'
			,data:'no data'
			,icon:'glyphicon glyphicon-comment'
			,visible:true
			,href:'#/comments'
			}
		,requests:{
			title:'Обращения'
			,bgColor:'bg-MediumSeaGreen'
			,data:'no data'
			,icon:'icon-addcomment'
			,visible:true
			,href:'#/requests'
			}
		,users:{
			title:'Регистрированных пользователей'
			,bgColor:'bg-DarkCyan'
			,data:'no data'
			,icon:'icon-user'
			,visible:true
			,href:'#/users'
			}
		,pages:{
			title:'Страниц на сайте'
			,bgColor:'bg-SandyBrown'
			,data:'no data'
			,icon:'icon-branch'
			,visible:true
			,href:'#/tree'
			}
		//~ ,visitors:{
			//~ title:'Посетители'
			//~ ,bgColor:'bg-DarkTurquoise'
			//~ ,data:'no data'
			//~ ,icon:'icon-groups-friends'
			//~ ,visible:true
			//~ }
		};
	
	
	
	self.rowClick = function(module,id)
	{
		location.hash = '#/'+module+'/'+id;
	}
	
	
	
	self.getInfo = function()
	{
		if (location.hash.toString().indexOf('#/default') >= 0)
			api.post({'action':"getInfo"}).then(function(d)
			{
				self.data = d;
				var widgets = d.widgets;
				
				self.widgets.pages.data = widgets.pages[0].amount;
				
				self.widgets.comments.data = widgets.comments.today+' / '+widgets.comments.all;
				self.widgets.requests.data = widgets.requests.today+' / '+widgets.requests.all;
				self.widgets.users.data = widgets.users.today+' / '+widgets.users.all;
				self.widgets.orders.data = widgets.orders.today+' / '+widgets.orders.all;
				
				//~ var newOrders = 0;
				//~ var allOrders = 0;
				//~ for (var n in widgets.orders)
					//~ if (widgets.orders[n].status == 'новый')
						//~ newOrders = widgets.orders[n].amount
					//~ else
						//~ allOrders += parseInt(widgets.orders[n].amount)
					//~ 
				//~ self.widgets.orders.data = newOrders+' / '+allOrders;
				self.updateCharts();
			});
	}
	setInterval(self.getInfo, self.dataRenew*1000);
	self.getInfo();
	
	
	self.updateCharts = function()
	{
		var cpu = self.data.widgets.server.cpu;
		var disk = self.data.widgets.server.disk.split(',');
		var memory = self.data.widgets.server.memory.split(',');
		var donutWidth = 5;
		
		new Chartist.Pie('#cpu-chart', {
			series: [cpu, 100-cpu]
		}, {
			donut: true,
			donutWidth: donutWidth,
			startAngle: 210,
			total: 100,
			showLabel: false
		});
		new Chartist.Pie('#memory-chart', {
			series: [memory[2], 100-memory[2]]
		}, {
			donut: true,
			donutWidth: donutWidth,
			startAngle: 210,
			total: 100,
			showLabel: false
		});
		new Chartist.Pie('#disk-chart', {
			series: [disk[2], 100-disk[2]]
		}, {
			donut: true,
			donutWidth: donutWidth,
			startAngle: 210,
			total: 100,
			showLabel: false
		});
		
		return;
		
		new Chartist.Bar('#server-charts', {
			labels: ['CPU', 'Mem', 'Disk'],
			series: [
				[100-cpu, 100-memory[2], 100-disk[2]],
				[cpu, memory[2], disk[2]]
			]
		}, {
			stackBars: true,
			horizontalBars: true,
			axisX: {
				labelInterpolationFnc: function(value) {
					return '';
				}
			}
		}).on('draw', function(data) {
			if(data.type === 'bar') {
				data.element.attr({
					style: 'stroke-width: 10px'
				});
			}
		});
		
		
		return;
		new Chartist.Pie('#cpu-chart', {
			series: [cpu, 100-cpu]
		}, {
			donut: true,
			donutWidth: 20,
			startAngle: 210,
			total: 100,
			showLabel: false
		});
		new Chartist.Pie('#memory-chart', {
			series: [memory[2], 100-memory[2]]
		}, {
			donut: true,
			donutWidth: 20,
			startAngle: 210,
			total: 100,
			showLabel: false
		});
		new Chartist.Pie('#disk-chart', {
			series: [disk[2], 100-disk[2]]
		}, {
			donut: true,
			donutWidth: 20,
			startAngle: 210,
			total: 100,
			showLabel: false
		});
	}
	
}
