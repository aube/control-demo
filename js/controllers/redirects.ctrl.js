
function RedirectsController(mainService,api,$location)
{
	var self = this;
	
	self.data = {};
	self.currentFilters = false;
	self.filters = [
		{'header':'from','alias':'from','data':null},
		{'header':'to','alias':'to','data':null},
		{'header':'method','alias':'method','data':null}
	];
	
	self.header = mainService.getActiveSectionHeader('redirects');
	
	self.onChangeFilter = mainService.onChangeFilter;
	
	self.filtersUpdate = function()
	{
		for (var f in self.filters)
		{
			var filter = self.filters[f];
			self.filters[f].data = $location.search()[filter.alias];
		};
	}
	
	self.editClick = function(id)
	{
		self.redirect = self.rows[id];
		$('#modal-redirect').modal('show');
	};
	
	self.addClick = function()
	{
		self.redirect = {};
		$('#modal-redirect-add').modal('show');
	};
	
	
	self.delClick = function(id)
	{
		self.redirect = self.rows[id];
		api.post({'action':'delRedirect', 'md5_from':self.rows[id].md5_from}).then(function(d) {
			self.currentFilters = false;
		});
	};
	
	self.saveRedirect = function(add)
	{
		var options = $.extend({
			action: 'saveRedirect'
		}, self.redirect);
		
		api.post(options).then(function(d) {
			self.currentFilters = false;
			if (add)
				$('#modal-redirect-add').modal('hide');
		});
	};
	
	
	//get data
	
	self.getData = function(){
		
		var filters = JSON.stringify($location.search());
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			self.currentFilters = filters;
			api.post({'action':'getRedirects','order':'dt-','filter':filters}).then(function(d) {
				self.data = d;
				self.rows = d.rows;
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				self.filtersUpdate();
				
				//pager
				mainService.setRowsAmount(d.found_rows);
				
			});
		}
	}
	mainService.getData = self.getData;
	
}
