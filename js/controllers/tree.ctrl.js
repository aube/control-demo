
function TreeController(mainService, api, $location,$route,$scope)
{
	var self = this;
	
	self.types = mainService.types;
	self.currentFilters = false;
	self.tree = [];
	self.breadCrambs = [];
	self.linkMode = false;
	
	self.linkModeParameters = [
		{title:'Все ресурсы',param:'allResourses'},
		{title:'Нигде не размещенные',param:'onlyNotConnectedResourses'},
		{title:'Ресурсы в активной группе',param:'onlyActiveNodeResourses'}];
	self.linkModeActiveParameter = self.linkModeParameters[0];
	
	
	self.changeLinkModeParam = function(param)
	{
		if (self.linkModeActiveParameter != param)
		{
			self.linkModeActiveParameter = param;
			self.currentFilters = false;
		}
	}
	
	
	////////////////////////////
	//tree
	
	//tree functions
	//получение узла, подчиненных ветвей и цепочки предков
	self.getTree = function(activeNodeId)
	{
		
		var params = {'action':'getTree','id':activeNodeId};
		api.post(params).then(function(data)
		{
			self.tree.push(data.tree);
			self.setBreadCrambs(data.activeNode);
			self.selectNode(self.breadCrambs[self.breadCrambs.length-1]);
		});
		
	};
	
	
	//рассчет цепочки узлов по пути
	self.setBreadCrambs = function(node){
		
		var path = node.path+'/'+node.alias
		path = path.replace('//','/').split('/');
		
		self.breadCrambs = [];
		
		var tree = self.tree;
		for(var a in path)
		{
			var alias = path[a];
			alias = alias==''?'index':alias;
			
			for (var n in tree)
			{
				var node = tree[n];
				if (node.alias == alias)
				{
					self.breadCrambs.push(node);
					tree = node.nodes;
					break;
				}
			}
		}
	}
	
	
	
	//получение узла и подчиненных ветвей
	self.getNode = function(node, callback)
	{
		
		var params = {'action':'getNode','id':node.id};
		
		api.post(params).then(function(data)
		{
			
			angular.extend(node,data);
			if (callback)
				callback();
		});
	};
	
	
	//выбор узла
	self.selectNode = function(node)
	{
		
		self.activeNode = node;
		self.activePath = node.path+(node.path>'/'?'/':'')+node.alias;
		var search = $location.search();
		search.parent = node.id;
		$location.search(search);
		if (node != self.breadCrambs[self.breadCrambs.length-1])
			self.setBreadCrambs(node);
		
	};
	
	
	//переключение узла
	self.toggleTreeNode = function(node,callback){
		
		if (node.open === undefined)
		{
			self.getNode(node,callback);
			node.open = true;
		}
		else
			node.open = !node.open;
		
	}
	
	
	//tree activate
	self.getTree($location.search().parent || 1);
	
	//tree
	////////////////////////////
	
	
	////////////////////////////
	//list
	
	//обработка клика по строке в списке
	self.selectRow = function(row){
		
		if (!row.id)
			return;
		
		
		if (self.activeNode.nodes == undefined)
		{
			self.toggleTreeNode(self.activeNode,function(){
				self.selectRow(row);
			});
		}
		self.activeNode.open = true;
		self.setBreadCrambs(row);
		self.selectNode(self.breadCrambs[self.breadCrambs.length-1]);
		
	};
	
	//list
	////////////////////////////
	
	
	
	
	self.getData = function(){
		
		var filters = $location.search();
		
		//if (self.mode=='sort' && !filters.parent)
		//	return;
		
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			
			var params = {};
			
			if (self.linkMode)
			{
				params = {'action':'getResoursesToLink',
					'filter':JSON.stringify(filters),
					'type':self.activeNode.type
					
					};
				params[self.linkModeActiveParameter.param] = true;
			}
			else
			{
				params = {'action':'getListChilds','filter':JSON.stringify(filters)};
			}
			
			
			self.currentFilters = filters;
			
			api.post(params).then(function(d)
			{
				self.data = d;
				self.rows = d.rows;
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				if (d.slave_types && d.slave_types != self.slave_types)
					self.slave_types = d.slave_types;
				
				
				//подчиненные страницы
				if (!self.linkMode)
				{
					self.sortableChildsUpdate();
				}
				else
				{
					self.sortableChildsDisable();
				}
				
				
				//pager
				mainService.setRowsAmount(d.found_rows);
			});
		}
	}
	mainService.getData = self.getData;
	
	
	
	
	////////////////
	//list sortable
	
	
	self.sortableChildsDisable = function()
	{
		$( "#sortableChilds" ).sortable( "disable" );
	}
	
	self.sortableChildsUpdate = function()
	{
		console.log('sorts');
		
		$( "#sortableChilds" ).sortable({
			start: function(event, ui){
				event.stopPropagation();
			}
			,update: function(event, ui){
				var id=$(ui.item[0]).attr('data-id');
				var after=$(ui.item[0]).prev().attr('data-id');
				var sortafter = $(ui.item[0]).prev().attr('data-sort') || 1;
				
				for (var t in self.rows)
				{
					if (self.rows[t].id == id)
					{
						
						if (after == undefined || sortafter>1)
						{
							self.rows[t].sort = sortafter+1;
							$(ui.item[0]).attr('data-sort',sortafter+1);
						}else{
							self.rows[t].sort = 1;
							$(ui.item[0]).attr('data-sort',1);
						}
					}
				}
				api.post({'action':'sortPosition','parent':self.activeNode.id,'id':id,'after':after});
			}
		});
		$( "#sortableChilds" ).sortable( "enable" );
		$( "#sortableChilds" ).disableSelection();
		
	};
	
	self.sortTop = function(e,id)
	{
		api.post({'action':'sortTop','parent':self.activeNode.id,'id':id}).then(
			function(){
				self.currentFilters=false;
			}
		);
		
	}
	
	self.sortRelease = function(e,id)
	{
		
		api.post({'action':'sortRelease','parent':self.activeNode.id,'id':id}).then(
		
			function(){
				self.currentFilters=false;
			}
		);
	}
	
	//list sortable
	////////////////
	
	
	
	
	////////////////
	//connections pages
	
	
	self.connectPage = function(node)
	{
		
		var params = {
				'type':node.type,
				'idtype':node.idtype,
				'alias':node.alias,
				'parent':$location.search().parent,
				'action':'connectPage'
			};
		api.post(params).then(
			function(result)
			{
				if(result)
					node.linked=1;
			}
		);
		
	}
	
	self.disconnectPage = function(node)
	{
		
		var params = {
				'parent':$location.search().parent,
				'idtype':node.idtype,
				'type':node.type,
				'action':'disconnectPage'
			};
		
		api.post(params).then(
			function(result)
			{
				if(result)
					node.linked=0;
			}
		);
		
	}
	
	//connections pages
	////////////////
	
	
	

	
	//переключатель режима (сортировка/прилинковка)
	self.modePage = {link:1,sort:1};
	self.changeMode = function()
	{
		var search = $location.search();
		if (search.page)
		{
			search.page = self.linkMode ? self.modePage.link : self.modePage.sort;
			$location.search(search);
		}
		
		self.linkMode = !self.linkMode;
		self.currentFilters = false;
	}
	
	
	
	//окно добавления ресурса
	self.addPageWindowShow = function(type)
	{
		mainService.onPageAdd = function(){self.currentFilters=false};
		mainService.addTypeCtrl.setType(type);
		mainService.addTypeCtrl.setParent(self.activeNode.id);
		$('#modal-addpage').modal('show');
	};
	
	
	
	
	//on document ready
	angular.element(document).ready(function () {
		
		function resizeTree(){
			var panelsContainer = angular.element('#panelsContainer');
			var tableContainer = angular.element('#tableContainer');
			var listFooter = angular.element('#listFooter');
			panelsContainer.css('height',(window.innerHeight - panelsContainer[0].offsetTop)+'px');
			tableContainer.css('height',(listFooter[0].offsetTop - tableContainer[0].offsetTop)+'px');
		}
		
		setTimeout(
			function(){
				resizeTree();
				angular.element('#panelsContainer').addClass('show');
			}
			,300);
		
		angular.element(window).bind('resize', resizeTree);
		
	});
	
}
