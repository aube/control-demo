
function MarketXMLController(mainService,api,$location)
{
	var self = this;
	
	
	self.newSetData = {
			name:'new set',
			id:0,
			description_length:250
		};
	self.data = {};
	self.sets = {};
	self.formats = {};
	self.addSet = function(){
		angular.copy(self.newSetData,self.data);
	}
	self.addSet();
	
	
	
	self.editSet = function(set){
		angular.copy(set,self.data);
	}
	
	self.saveSet = function(){
		api.post({'action':'saveMarketXMLSet','data':self.data}).then(function(d) {
			self.sets = d;
			if (self.data.id==0)
			{
				self.editSet(self.sets[0]);
			}
		});
	}
	
	//get data
	
	api.post({'action':'getMarketXMLStartValues'}).then(function(d) {
		self.sets = d.sets;
		self.formats = d.formats;
		self.newSetData.params = d.default_params;
		self.newSetData.format = self.formats[0];
	});
	
	self.currentParams = false;
	self.getData = function(){
		
		var params = JSON.stringify($location.search());
		
		if (params && params.id)
		if (self.currentParams != params)
		{
			self.currentParams = params;
			api.post({'action':'getMarketXMLSet','params':params}).then(function(d) {
				self.data = d;
			});
		}
	}
	mainService.getData = self.getData;
	
}
