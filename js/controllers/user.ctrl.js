
function UserController(mainService,api,$route)
{
	var self = this;
	
	self.id = $route.current.params.id;
	
	self.header = mainService.getActiveSectionHeader('users');
	
	self.getOperations = function()
	{
		self.currentAccountOperations = self.data.mail;
		api.post({'action':'getOperations','account':self.id}).then(function(d) {
			self.operations = d;
			$('#modal-operations').modal('show');
		});
		
	};
	
	
	//get data
	self.data = {};
	self.requestSend = false;
	self.getData = function(){
		
		if (self.requestSend===true)
			return;
		
		api.post({'action':'getUser','id':self.id}).then(function(d) {
			self.data = d;
		});
		self.requestSend = true;
		
	}
	mainService.getData = self.getData;
	
	
	self.save = function()
	{
		
		api.post({'action':'saveUser','data':self.data}).then(function(d) {
			location.hash = '/user/'+d.id;
		});
		
	}
}
