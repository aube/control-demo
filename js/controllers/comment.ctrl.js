
function CommentController(mainService,api,$route)
{
	var self = this;
	
	self.id = $route.current.params.id;
	
	self.header = mainService.getActiveSectionHeader('orders');
	
	//get data
	self.data = {};
	self.dataExists = false;
	self.getData = function(){
		
		if (self.dataExists===true)
			return;
		
		api.post({'action':'getComment','id':self.id}).then(function(d) {
			self.data = d;
		});
		self.dataExists = true;
		
	}
	mainService.getData = self.getData;
	
	
	self.delComment = function()
	{
		api.post({'action':'delComment','id':self.id}).then(function(d) {
			location.hash = '/comments';
		});
	}
	self.saveComment = function()
	{
		api.post({'action':'saveComment','data':self.data}).then(function(d) {
			//location.hash = '/comments';
		});
	}
}
