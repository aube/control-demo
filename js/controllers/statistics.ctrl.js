
function StatisticsController(mainService,api,$location)
{
	var self = this;
	
	self.header = mainService.getActiveSectionHeader('Статистика');
	
	self.names = {
		period:'Период',
		total:'Сумма',
		amount:'Кол-во',
		status:'Статус'
	};
	
	
	
	//menu
	self.currentMenu = $location.search()['menu'];
	self.menus = [
			{'name':'orders','header':'Заказы'},
			{'name':'users','header':'Пользователи'}
		];
	self.changeCurrentMenu = function(menu)
	{
		self.currentMenu = menu;
		$location.search({'menu':self.currentMenu});
	}
	
	
	self.ordersClick = function(ordersPeriod, orders){
		
		var dt = ordersPeriod.periodStart==ordersPeriod.periodEnd ? ordersPeriod.periodEnd : ordersPeriod.periodStart.substr(0,7);
		
		location.hash = '/orders?dt='+dt+'&status='+orders.status
	};
	
	self.requestsClick = function(){
		
		location.hash = '/requests'
	};
	
	
	////////////////
	//get data
	self.data = {};
	self.dataExists = false;
	
	self.getData = function()
	{
		
		if (self.dataExists !== false)
		{
			return;
		}
		
		api.post({'action':'getStatistics'}).then(function(d) {
			
			console.log(d);
			self.data = d;
			self.orders = d.orders;
			self.users = d.users;
			self.metrics = d.metrics;
			self.requests = d.requests.rows;
			self.dataExists = true;
			
		});
		
		self.dataExists = true;
	}
	
	mainService.getData = self.getData;
	//get data
	////////////////

	
	
}
