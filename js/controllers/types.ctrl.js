
function TypesController(mainService, api, $location,$route)
{
	var self = this;
	
	//types
	self.types = mainService.types;
	self.type = $route.current.params.type;
	
	
	self.typeName = 'Выбор типа';
	for (var n in self.types)
	{
		if (self.types[n].class == self.type)
		{
			self.typeName = self.types[n].name;
		}
	}
	
	
	
	self.newPage = {};
	self.data = {};
	
	
	self.header = mainService.getActiveSectionHeader('types');
	
	
	self.rowClick = function(type,idtype)
	{
		type = type || self.type;
		location.hash = '/type/'+type+'/'+idtype+'/resource';
	};
	
	
	
	//get data
	self.currentFilters = false;
	self.getData = function(){
		
		if (!self.type)
			return;
		
		var filters = JSON.stringify($location.search());
		var params = {};
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			
			self.currentFilters = filters;
			
			params = {'action':'getResoursesByType','type':self.type,'filter':filters};
			
			api.post(params).then(function(d)
			{
				self.data = d;
				self.rows = d.rows;
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				//self.filtersUpdate();
				
				//pager
				mainService.setRowsAmount(d.found_rows);
			});
		}
	}
	mainService.getData = self.getData;
	
	
	//окно добавления ресурса
	self.addPageWindowShow = function()
	{
		var type = {class:self.type,name:self.typeName};
		mainService.onPageAdd = function(){self.currentFilters=false};
		mainService.addTypeCtrl.setType(type);
		mainService.addTypeCtrl.setParent(false);
		$('#modal-addpage').modal('show');
	};
	
}
