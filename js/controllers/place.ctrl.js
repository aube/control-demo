
function PlaceController(mainService, api,$location,$route)
{
	var self = this;
	
	self.place = 
	self.id = self.menu = $route.current.params.id;
	
	
	//get data
	if (self.id>0)
	{
		var params = {'action':'getPlace','id':self.id};
		api.post(params).then(function(d) {
			self.place = d;
		});
	}else{
		self.place = {
			id:0,
			name:'',
			address:'',
			content:'',
			city:'',
			country:'',
			phone:'',
			url:''
		};
	}
	
	
	self.savePlace = function(){
		
		var params = {'action':'savePlace','params':self.place};
		api.post(params);
	}
	
	
	self.delPlace = function(){
		var params = {'action':'delPlace','params':self.place};
		api.post(params).then(function(d) {
			location.hash = '/places';
		});
		
	}
	
	
	
	
}
