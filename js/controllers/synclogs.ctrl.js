
function SyncLogsController(mainService, $route,api,$location)
{
	var self = this;
	
	self.header = mainService.getActiveSectionHeader('synclogs');
	
	self.data = {};
	self.menu = $route.current.params.menu;
	
	self.menus = [
			{'name':'queue','header':'Поступившие данные'},
			{'name':'results','header':'Результат синхронизации'}
		];
		
	
	
	
	//
	self.rowClick = function(row)
	{
		
		
		console.log(row.data);
		
		
	}
	
	
	
	//get data
	self.currentFilters = false;
	self.getData = function(){
		
		var filters = JSON.stringify($location.search());
		
		if (self.currentFilters != filters || self.currentFilters===false)
		{
			
			var action = self.menu == 'queue'?'getSyncQueue':'getSyncResults';
			
			
			self.currentFilters = filters;
			api.post({'action':action,'order':'dt-','filter':filters}).then(function(d) {
				self.data = d;
				self.rows = d.rows;
				
				self.rowsAmount = d.found_rows;
				self.pagesAmount = Math.floor(self.rowsAmount / self.limit);
				
				//pager
				mainService.setRowsAmount(d.found_rows);
				
			});
		}
	}
	mainService.getData = self.getData;
	
}


