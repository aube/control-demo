function directivePager()
{
	return {
		restrict: 'E',
		
		template:
			'<span class="ls-pager">'+
			'<span class="like_a glyphicon glyphicon-chevron-left" ng-click="pagerCtrl.prevPage()" ng-class="{disabled:pagerCtrl.currentPage==1}"></span>'+
			'<input type="text" id="inputpage" ng-model="pagerCtrl.currentPage" ng-change="pagerCtrl.onChangePage()" ng-model-options="{debounce:300}"> / {{pagerCtrl.pagesAmount}}'+
			'<span class="like_a glyphicon glyphicon-chevron-right" ng-click="pagerCtrl.nextPage()" ng-class="{disabled:pagerCtrl.pagesAmount==pagerCtrl.currentPage}"></span>'+
			'</span>',
		
		controller: function pagerController(mainService,$location)
				{
					var self = this;
					
					self.limit = 20;
					self.pagesAmount = 0;
					self.rowsAmount = 0;
					self.currentPage = self.currentPage = $location.search().page || 1;;
					
					setInterval(function(){
						self.currentPage = $location.search().page || 1;
					},200);
					
					
					self.setRowsAmount = function(amount)
					{
						self.rowsAmount = amount;
						self.pagesAmount = Math.floor(self.rowsAmount / self.limit)+1;
					}
					mainService.setRowsAmount = self.setRowsAmount;
					
					self.nextPage = function()
					{
						self.currentPage++;
						if (self.currentPage>self.pagesAmount)
							self.currentPage=self.pagesAmount;
						else
							self.onChangePage();
					}
					
					self.prevPage = function()
					{
						self.currentPage--;
						if (self.currentPage==0)
							self.currentPage=1;
						else
							self.onChangePage();
					}
					
					self.onChangePage = function()
					{
						
						self.currentPage = parseInt(self.currentPage)||1;
						if (self.currentPage==0)
							self.currentPage=1;
						if (self.currentPage>self.pagesAmount)
							self.currentPage=self.pagesAmount;
						
						//update params string
						var search = $location.search();
						search.page = self.currentPage;
						$location.search(search);
						
					}
					
				},
		controllerAs: 'pagerCtrl'
		
	};  
}

