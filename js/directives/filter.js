function directiveFilter($location)
{
	return {
		restrict: 'E',
		template: '<input value="" class="table-filter">',
		replace: true,
		link:function(scope, element, attrs)
		{
			var debounce = attrs.debounce || 500;
			var currentTO = false;
			
			var search = $location.search();
			if (search[attrs.name])
			{
				element.val(search[attrs.name]);
				element.addClass('active');
			}
			
			var changeFilters = function()
			{
				
				search = $location.search();
				
				if (currentTO)
					clearTimeout(currentTO);
				
				currentTO = setTimeout(
					function(){
						
						search[attrs.name] = element.val();
						if (search.page != undefined)
							search.page = 1;
						
						if (""==search[attrs.name])
						{
							delete(search[attrs.name]);
							element.removeClass('active');
						}
						else
						{
							element.addClass('active');
						}
						
						$location.search(search);
						
					}, debounce)
			};
			
			element.attr('placeholder',attrs.name);
			
			// on blur, update the value in scope
			element.bind('propertychange keyup paste', changeFilters);
			
		}
		
		
	};  
}

