function modalAddPageController(mainService, api)
{
	var self = this;
	
	self.type = false;
	self.parent = false;
	
	mainService.addTypeCtrl = self;
	
	
	self.setType = function(type)
	{
		self.modalHeader = 'Создать и добавить страницу типа '+type.name+':';
		self.type = type.class;
	}
	
	self.setParent = function(parent)
	{
		self.parent = parent;
	}
	
	
	
	
	self.addPage = function()
	{
		$('#modal-addpage').modal('hide');
		api.post({'action':'addPage','type':self.type,'alias':self.alias,'header':self.header,'parent':self.parent}).then(function(d) {
			clog(d);
			mainService.onPageAdd();
			//location.hash = '/page/'+self.type+'/'+d.idtype+'/common';
		});
	};
	
	self.onChangeHeader = function()
	{
		self.alias = translite_alias(self.header);
	};
	
}
