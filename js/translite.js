function translite_alias(str)
{
	var str=translite(str).replace(/[ \"\'\`\№\;\:\<\>\,\.\[\]\{\}\(\)\!\@\#\$\%\^\&\*\+]/g,"-");
	while (str!=str.replace(/--/g,"-"))
	{
		str=str.replace(/--/g,"-");
	}
	return str.toLowerCase();
}


function translite(str)
{
	var a=alphabet();
	return str_replace(a.from, a.to, str);
}


function alphabet(lang){
	var arr=new Array();
	var from=new Array();
	var to=new Array();
	lang = lang || 'rus';
	
	arr.push(new Array("Щ","Sch"));
	arr.push(new Array("Ы","Yi"));
	arr.push(new Array("Ю","Yu"));
	arr.push(new Array("Я","Ya"));
	arr.push(new Array("Ё","Yo"));
	arr.push(new Array("Ц","Ts"));
	arr.push(new Array("Ч","Ch"));
	arr.push(new Array("Ш","Sh"));
	arr.push(new Array("А","A"));
	arr.push(new Array("Б","B"));
	arr.push(new Array("В","V"));
	arr.push(new Array("Г","G"));
	arr.push(new Array("Д","D"));
	arr.push(new Array("Е","E"));
	arr.push(new Array("Ж","J"));
	arr.push(new Array("З","Z"));
	arr.push(new Array("И","I"));
	arr.push(new Array("Й","Y"));
	arr.push(new Array("К","K"));
	arr.push(new Array("Л","L"));
	arr.push(new Array("М","M"));
	arr.push(new Array("Н","N"));
	arr.push(new Array("О","O"));
	arr.push(new Array("П","P"));
	arr.push(new Array("Р","R"));
	arr.push(new Array("С","S"));
	arr.push(new Array("Т","T"));
	arr.push(new Array("У","U"));
	arr.push(new Array("Ф","F"));
	arr.push(new Array("Х","H"));
	arr.push(new Array("Ъ",""));
	arr.push(new Array("Ь",""));
	arr.push(new Array("Э","E"));
	arr.push(new Array("щ","sch"));
	arr.push(new Array("ю","yu"));
	arr.push(new Array("я","ya"));
	arr.push(new Array("ы","yi"));
	arr.push(new Array("ц","ts"));
	arr.push(new Array("ч","ch"));
	arr.push(new Array("ш","sh"));
	arr.push(new Array("ё","yo"));
	arr.push(new Array("а","a"));
	arr.push(new Array("б","b"));
	arr.push(new Array("в","v"));
	arr.push(new Array("г","g"));
	arr.push(new Array("д","d"));
	arr.push(new Array("е","e"));
	arr.push(new Array("ж","j"));
	arr.push(new Array("з","z"));
	arr.push(new Array("и","i"));
	arr.push(new Array("й","y"));
	arr.push(new Array("к","k"));
	arr.push(new Array("л","l"));
	arr.push(new Array("м","m"));
	arr.push(new Array("н","n"));
	arr.push(new Array("о","o"));
	arr.push(new Array("п","p"));
	arr.push(new Array("р","r"));
	arr.push(new Array("с","s"));
	arr.push(new Array("т","t"));
	arr.push(new Array("у","u"));
	arr.push(new Array("ф","f"));
	arr.push(new Array("х","h"));
	arr.push(new Array("ъ",""));
	arr.push(new Array("ь",""));
	arr.push(new Array("э","e"));

	for (var key in arr){
		from.push(arr[key][(lang=="rus"?0:1)]);
		to.push(arr[key][(lang=="rus"?1:0)]);
	}
	return {'from':from,'to':to};
}


function str_replace (search, replace, subject) {
	// Replace all occurrences of the search string with the replacement string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Gabriel Paderni

	if(!(replace instanceof Array)){
		replace=new Array(replace);
		if(search instanceof Array){//If search	is an array and replace	is a string, then this replacement string is used for every value of search
			while(search.length>replace.length){
				replace[replace.length]=replace[0];
			}
		}
	}

	if(!(search instanceof Array))search=new Array(search);
	while(search.length>replace.length){
		//If replace	has fewer values than search , then an empty string is used for the rest of replacement values
		replace[replace.length]='';
	}

	if(subject instanceof Array){
		//If subject is an array, then the search and replace is performed with every entry of subject , and the return value is an array as well.
		for(k in subject){
			subject[k]=str_replace(search,replace,subject[k]);
		}
		return subject;
	}

	for(var k=0; k<search.length; k++){
		var i = subject.indexOf(search[k]);
		while(i>-1){
			subject = subject.replace(search[k], replace[k]);
			i = subject.indexOf(search[k],i);
		}
	}

	return subject;

}
