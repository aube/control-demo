function api($http)
{
	var self = this;
	var url = './api.php';
	
	
	self.get = function(params)
	{
		self.preloaderShow();
		
		var data = $http.get(url, params).
			then(
				function (response)
				{
					console.log(response);
					
					self.preloaderHide();
					
					if (flashMessages)
					{
						self.flashMessages(response.data);
					}
					
					return response.data;
				}
			);
			
		return data;
	}
	
	self.post = function(params)
	{
		self.preloaderShow();
		
		var data = $http.post(url, params).
			then(
				function (response)
				{
					console.log(response);
					
					self.preloaderHide();
					
					if (flashMessages)
					{
						//self.flashMessages(response.data);
					}
					
					return response.data;
				},
				function(error)
				{
					if (error.status === 500)
					{
						
						self.preloaderHide();
						
						if (error.data.redirect)
						{
							location.replace(error.data.redirect);
							return false;
						}
						
						if (flashMessages && error.data)
						{
							flashMessages.add('<p>'+error.data.join('</p><p>')+'</p>',0,'error');
						}
						
					}
					return false;
				}
			);
			
		return data;
	}
	
	
	self.flashMessages = function(responseData)
	{
		if (responseData.error)
		{
			flashMessages.add('<p>'+responseData.error+'</p>',0,'error');
		}
		else if (responseData.errors)
		{
			flashMessages.add('<p>'+responseData.errors.join('</p><p>')+'</p>',0,'error');
		}
		else if(responseData.success)
		{
			flashMessages.add('<p>'+responseData.success+'</p>',1000,'success');
		}
		else
			flashMessages.add('<p>Данные получены</p>',1000,'success');
		
	}
	
	
	self.preloaderShow = function()
	{
		$('#preloader').show();
	}
	
	
	self.preloaderHide = function()
	{
		$('#preloader').hide();
	}
	
	
}


/*
//перенести сюда flashMessages

function testInterceptor() {
  return {
    request: function(config) {
      return config;
    },

    requestError: function(config) {
      return config;
    },

    response: function(res) {
      return res;
    },

    responseError: function(res) {
      return res;
    }
  }
}

angular.module('app', [])
.factory('testInterceptor', testInterceptor)
.config(function($httpProvider) {
  $httpProvider.interceptors.push('testInterceptor');
})
.run(function($http) {
  $http.get('http://test-routes.herokuapp.com/test/hello')
    .then(function(res) {
      console.log(res.data.message)
    })
})
*/
