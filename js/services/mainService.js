
function mainService($http, $location)
{
	var mainService = {};
	
	//load:
	mainService.systemInfo = systemInfo;
	mainService.serverURL = './api.php';
	
	mainService.types = availableTypes;
	
	mainService.getTypenameByClassname = function(name)
	{
		for(var n in mainService.types)
		{
			if (mainService.types[n].class==name)
				return mainService.types[n].name;
		}
		flashMessages.add('nor found type: '+type,0,'error');
		return false;
	};
	
	
	
	mainService.availableModules = availableModules;
	mainService.availableSections = availableModules;
	
	//convert section to [alias]=header;
	mainService.sectionsByAlias = {};
	for (var k in mainService.availableSections) 
		mainService.sectionsByAlias[mainService.availableSections[k].alias] = mainService.availableSections[k].header;
	
	
	mainService.getActiveSectionHeader = function(alias)
	{
		if (mainService.sectionsByAlias[alias])
			return mainService.sectionsByAlias[alias];
		else
			return alias;
	}
	
	
	mainService.checkModuleAccess = function(module)
	{
		return (typeof mainService.sectionsByAlias[module] == 'undefined')?false:true;
	};
	
	
	
	
	
	
	//pagerController
	//mainService.setRowsAmount = function(){};
	
	//pagerController
	mainService.getFilters = function(){return false;};
	mainService.setFilters = function(){return false;};
	mainService.updateFilters = function(){return false;};
	mainService.getData = function(){return false;};
	mainService.getCurrentPage = function(){return false;};
	
	
	mainService.onChangeFilter = function(input)
	{
		var filter = input.filter;
		var search = $location.search();
		if (filter.data=='')
			delete(search[filter.alias]);
		else
			search[filter.alias] = filter.data;
		$location.search(search);
	}
	
	
	
	api.post = function(params)
	{
		var data = $http.post(mainService.serverURL, params
			).then(function (response) {
				// The then function here is an opportunity to modify the response
				console.log(response);
				
				
				if (flashMessages)
				{

						
						
					if (response.data.error)
					{
						flashMessages.add('<p>'+response.data.error+'</p>',0,'error');
					}
					else if (response.data.errors)
					{
						flashMessages.add('<p>'+response.data.errors.join('</p><p>')+'</p>',0,'error');
					}
					else if(response.data.message)
					{
						flashMessages.add('<p>'+response.data.message+'</p>',0,'message');
					}
					else if(response.data.success)
					{
						flashMessages.add('<p>'+response.data.success+'</p>',1000,'success');
					}
					else
						flashMessages.add('<p>Данные получены</p>',1000,'success');
					
				}
				
				// The return value gets picked up by the then in the controller.
				return response.data;
			});
		return data;
	}
	
	setInterval(function(){mainService.getData()},100);
	
	
	
	return mainService;
};

