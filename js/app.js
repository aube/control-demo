'use strict';

/* App Module */





angular.module('controlApp', [
	'ngRoute'
	,'angularFileUpload'
	,'ui.tinymce'
	//,'ngSanitize'
	//,'MassAutoComplete'
	//,'as.sortable'
	//,'ngCookies'
	//,'restangular'
	])

	.config(['$routeProvider',routeConfig])
	.factory('mainService',mainService)
	.service('api',api)
	.directive('filter', ['$location' , directiveFilter])
	.directive('pager', ['$location' , directivePager])
	.directive('modalAddPage', modalAddPageDirective)
	.controller('topMenuController',topMenuController)
	
	.directive('stringToNumber', function() {
	  return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
		  ngModel.$parsers.push(function(value) {
			return '' + value;
		  });
		  ngModel.$formatters.push(function(value) {
			return parseFloat(value, 10);
		  });
		}
	  };
	})
	
	//image nof found
	.directive('fallbackSrc', function () {
		var fallbackSrc = {
			link: function postLink(scope, element) {
			element.bind('error', function() {
				angular.element(this).attr("src", "./css/no-image-found.jpg");
			});
			}
		}
		return fallbackSrc;
	})
	
	//objects sort in ng-repeat
	.filter('orderObjectBy', function() {
		return function(items, field, reverse) {
			var filtered = [];
			
			angular.forEach(items, function(item) {
				filtered.push(item);
			});
			
			if(field)
				filtered.sort(function (a, b) {
					return (a[field] > b[field] ? 1 : -1);
				});
			if(reverse) filtered.reverse();
			return filtered;
		};
	})
	;




function modalAddPageDirective(){  
	return { 
		restrict: 'E',
			templateUrl: 'templates/modal/addPage.html',
			controller: modalAddPageController,
			controllerAs: 'modalAddPageCtrl'
		}; 
}



function filtersDirective(){  
	return {
		restrict: 'E',
		templateUrl: 'templates/filters.html',
		controller: filtersController,
		controllerAs: 'filtersCtrl'
	}; 
}





function topMenuController(mainService)
{
	
	var self = this;
	
	self.siteName = mainService.systemInfo.sitename;
	self.siteURI = mainService.systemInfo.uri;
	self.sections = availableModules;
	self.menuStructure = menuStructure;
}


function BannersController(mainService){
	mainService.updateActiveTopMenuSection('banners');
}

function LogsController(mainService){
	mainService.updateActiveTopMenuSection('logs');
}
function DeletedController(mainService){
	mainService.updateActiveTopMenuSection('deleted');
}







	//сравнение данных для флага "сохранить"
	function equalObj(o0,o1)
	{
		
		if (typeof(o0) != 'object' && typeof(o1) != 'object')
			return null;
		for (var i in o0)
			if (o0.hasOwnProperty(i) && o0[i] != o1[i]){
				//~ console.log(o0[i]);
				//~ console.log(o1[i]);
				return false;
			}
		for (var i in o1)
			if (o1.hasOwnProperty(i) && o0[i] != o1[i])
				return false;
		return true;
	}
