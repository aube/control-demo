<?php

include_once(dirname(__FILE__).'/../init.php');




if (User::$id)
{
	include_once(dirname(__FILE__).'/class.controlapi.php');
	$API = new ControlAPI();
	$availableModules = json_encode($API->getModules());
	$availableControllers = json_encode($API->getControllers());
	$availableTypes = json_encode($API->getTypes());
	
	//добавляем в меню страницы типов
	//переделать в конфиг + права
	$menuStructure = $API->getMenu();
	$menuStructure = json_encode($menuStructure);
	
	$systemInfo = json_encode($API->getSystemInfo());
	//$methods = json_encode($API->getMethods());
	
	
	$typesInterconnection = json_encode(getV('typesInterconnection'));
	
	$typesEditAccess = getV('typesEditAccess');
	$typesEditAccess = json_encode($typesEditAccess[User::$id]);
	
	$typesAddAccess = getV('typesAddAccess');
	$typesAddAccess = json_encode($typesAddAccess[User::$id]);
	
	$typesDelAccess = getV('typesDelAccess');
	$typesDelAccess = json_encode($typesDelAccess[User::$id]);
	
	
	include_once(dirname(__FILE__).'/index.tpl');
}
else
{
	include_once(dirname(__FILE__).'/signin.tpl');
}



