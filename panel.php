<?php

include_once(dirname(__FILE__).'/../init.php');


$controlAccess = getV('controlaccess');
$controlAccess = $controlaccess[User::$id];

if (User::$id && User::isAdmin() && $controlAccess)
{
	
	$page = json_decode($_REQUEST['page'],1);
	$id = $page['id'];
	$type =  $page['link']>0 ? 'link' : $page['type'];
	$idtype = $page['link']>0 ? $page['link'] : $page['idtype'];
	
	
	$typesEditAccess = getV('typesEditAccess');
	$typesEditAccess = $typesEditAccess[User::$id];
	
	$typesAddAccess = getV('typesAddAccess');
	$typesAddAccess = $typesAddAccess[User::$id];
	
	$typesDelAccess = getV('typesDelAccess');
	$typesDelAccess = $typesDelAccess[User::$id];
	
	$editAccess = in_array($type,$typesEditAccess);
	$delAccess = in_array($type,$typesDelAccess);
	
	
	$addTypes = array();
	
	if (count($typesAddAccess))
	{
		$typesInterconnection = getV('typesInterconnection');
		
		foreach ($typesInterconnection as $slave=>$types)
		{
			if (in_array($type,$types) && in_array($slave,$typesAddAccess))
			{
				$addTypes[$slave] = Types::getTypeName($slave);
			}
		}
	}
	
}
?>

<link rel="stylesheet" href='<?=CONTROL_URL?>/css/panel.css'>
<link rel="stylesheet" href='<?=CONTROL_URL?>/css/flash-messages.css'>
<link rel="stylesheet" href='<?=CONTROL_URL?>/_css/jquery.fileupload.css'>
<div id='controlPanel'>
<?if ($controlAccess){?>
	<a class='btn btn-default' href='<?=CONTROL_URL?>' title='control panel'>
		<img src='<?=CONTROL_URL?>/css/button-control.png'>
	</a>
<?}?>
<?if ($editAccess){?>
	<a class='btn btn-default' href='<?=CONTROL_URL?>/#/type/<?=$type?>/<?=$idtype?>/resource' title='edit resourse'>
		<img src='<?=CONTROL_URL?>/css/button-edit-inside.png'>
	</a>
	<span class='btn btn-default' onclick='panel.editModeEnabled(this)' title='edit resourse'>
		<img src='<?=CONTROL_URL?>/css/button-edit.png'>
	</span>
<?}?>


<?if (count($addTypes)){?>
	<!-- Single button -->
	<div class="btn-group dropup" title='add + connect page'>
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<img src='<?=CONTROL_URL?>/css/button-add.png'>
		</button>
		<ul class="dropdown-menu">
			<?foreach($addTypes as $class=>$name){?>
			<li><a onclick='panel.addAndConnectPage("<?=$class?>","<?=$name?>",<?=$id?>)'><?=$name?></a></li>
			<?}?>
		</ul>
	</div>
<?}?>

<?if ($delAccess && $page['id'] && $page['childs']==0){?>
	<span class='btn btn-default' onclick='panel.delPage()' title='delete page'>
		<img src='<?=CONTROL_URL?>/css/button-delete.png'>
	</span>
<?}?>

	<div id='controlPanelTrigger'>
	</div>
</div>

<?include_once(CONTROL.'/templates/modal/addPage.html')?>




<div id='uploadFilesContainer'>
	<!-- The fileinput-button span is used to style the file input field as button -->
	<span class="btn btn-success fileinput-button">
		<i class="glyphicon glyphicon-plus"></i>
		<span>Добавить изображения...</span>
		<!-- The file input field used as target for the file upload widget -->
		<input id="fileupload" type="file" name="imagesupload[]" multiple>
	</span>
	<br>
	<br>
	<!-- The global progress bar -->
	<div id="progress" class="progress">
		<div class="progress-bar progress-bar-success"></div>
	</div>
</div>



<!--script src='<?=CONTROL_URL?>/js/jquery.flash_messages.js' /-->
<script src='<?=CONTROL_URL?>/_js/jquery.ui.widget.js' />
<script src='<?=CONTROL_URL?>/_js/jquery.fileupload.js' />
<script src='<?=CONTROL_URL?>/js/translite.js' />
<script src='<?=CONTROL_URL?>/_js/tinymce433/tinymce.min.js' />
<script src='<?=CONTROL_URL?>/js/panel.js' />


