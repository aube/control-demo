<?php

function imagesstorageControlAPIMethods()
{
	
	return array('getIStorageTypes','getIStorageList','editIStorageImage','delIStorageImage','uploadImageToStorage','addIStorageType','editIStorageType');
	
}

function imagesstorageModuleName()
{
	
	return 'Изображения, Баннеры';
	
}


function getIStorageTypes()
{
	
	$types = ImagesStorage::getTypes();
	return array_values($types);
}


function getIStorageList()
{
	
	if ($_REQUEST['type'])
	{
		$ImagesStorage = new ImagesStorage($_REQUEST['type']);
		$images = $ImagesStorage->getList();
		return array(
			'images'=>array_values($images),
			'upload_max_filesize'=>ini_get('upload_max_filesize')
		);
	
	}
	
}


function editIStorageImage()
{
	$IStorage = new ImagesStorage($_REQUEST['type']);
	
	$name = $_REQUEST['name'];
	unset($_REQUEST['type']);
	unset($_REQUEST['action']);
	
	return $IStorage->editImage($name, $_REQUEST);
	
}


function delIStorageImage()
{
	$IStorage = new ImagesStorage($_REQUEST['type']);
	$IStorage->delImage($_REQUEST['name']);
	
	$types = ImagesStorage::getTypes();
	return array_values($types);
}



function addIStorageType()
{
	$IStorage = new ImagesStorage($_REQUEST['name']);
	unset($_REQUEST['action']);
	unset($_REQUEST['limit']);
	
	$IStorage->addType($_REQUEST);
	
	$types = ImagesStorage::getTypes();
	return array_values($types);
}

function editIStorageType()
{
	$IStorage = new ImagesStorage($_REQUEST['name']);
	unset($_REQUEST['action']);
	unset($_REQUEST['limit']);
	
	$IStorage->editType($_REQUEST);
	
	$types = ImagesStorage::getTypes();
	return array_values($types);
}


function uploadImageToStorage()
{
	$ImagesStorage = new ImagesStorage($_REQUEST['type']);
	return $ImagesStorage->uploadImages();
}



