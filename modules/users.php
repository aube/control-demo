<?php

function usersControlAPIMethods()
{
	return array('getUsers','getUser','saveUser');
}

function usersModuleName()
{
	return 'Пользователи';
}

function usersAdditionalControllers()
{
	return array('user');
}

function saveUser()
{
	$data = $_REQUEST['data'];
	return array('id'=>User::saveUser($data));
}



function getUsers()
{
	$DB=DB::getDB();
	
	if (class_exists('Billing')){
		$Billing=new Billing();
		$sql='
		select SQL_CALC_FOUND_ROWS
			au.*,
			ai.dtreg,
			ai.phone,
			ai.name,
			ai.surname,
			ai.family,
			format(if(bb.summ is null, 0 , bb.summ/'.($Billing->decimalFactor).'),2) summ
		
		from '.TABLEPREFIX.'a_user au
		left join '.TABLEPREFIX.'a_info ai on au.id=ai.id
		left join '.TABLEPREFIX.'billing_balance bb on au.id=bb.account
		'.Tools::atables_where().'
		'.Tools::atables_orderBy().'
		limit '.$_REQUEST['page']*$_REQUEST['limit'].','.$_REQUEST['limit'];
	}else{
		$sql='
		select SQL_CALC_FOUND_ROWS
			au.*,
			ai.phone,
			ai.name,
			ai.surname,
			ai.family
		from '.TABLEPREFIX.'a_user au
		left join '.TABLEPREFIX.'a_info ai on au.id=ai.id
		'.Tools::atables_where().'
		'.Tools::atables_orderBy().'
		limit '.$_REQUEST['page']*$_REQUEST['limit'].','.$_REQUEST['limit'];
	}
	$arr['rows']=$DB->qry2arr($sql);
	$arr['found_rows']=$DB->fr();
	$arr['columns']=$DB->columns(TABLEPREFIX.'a_user');
	$arr['sql']=$sql;
	
	return $arr;
}


function getUser()
{
	
	
	$id=(int)$_REQUEST['id'];
	
	$arr=getUserEditFields($id);
	$arr['iiiddd'] = $id;
	
	if ($id>0){
		
		
		if (class_exists('Billing'))
		{
			$Billing = new Billing();
			$arr['balance'] = $Billing->getBalance($id);
		}
		
		if (class_exists('Address'))
		{
			$Addresses = new Address();
			$Addresses->user_id = $id;
			$arr['addresses'] = $Addresses->listAddresses();
		}
		
		if (class_exists('Orders'))
		{
			$Orders=new Orders();
			$arr['orders'] = $Orders->getOrdersById($id);
		}
		
		if (class_exists('Billing'))
		{
			$Billing=new Billing();
			$arr['billing'] = $Billing->getOperations($id);
		}
		
		if (class_exists('Basket'))
		{
			$Basket=new Basket();
			$Basket->user_id = $id;
			$arr['basket'] = $Basket->getItems();
		}
		
	}
	return $arr;
}



function getUserEditFields($id){
	if ($id==0){
		$arr=array();
		$arr['id']=0;
		$arr['mail']='';
		$arr['nick']='';
		$arr['pass']='';
		$arr['groups']='user';
		$arr['name']='';
		$arr['surname']='';
		$arr['family']='';
		$arr['dr']='';
		$arr['age']='';
		$arr['gender']=0;
		$arr['email']='';
		$arr['icq']='';
		$arr['jabber']='';
		$arr['skype']='';
		$arr['phone']='';
		$arr['city']='';
		$arr['site']='';
		$arr['school']='';
		$arr['work']='';
		$arr['status']='';
		$arr['interest']='';
		$arr['about']='';
		$arr['params']='';
		
	}else{
		$arr=User::info($id);
	}
	return $arr;
}
