<?php

function billingHistoryControlAPIMethods()
{
	return array('getHistoryPayments','addOperation');
}


function billingHistoryModuleName()
{
	return 'Биллинг - транзакции';
}


function getHistoryPayments(){
	$Billing=new Billing();
	return $Billing->getHistoryList();
}


function addOperation(){
	
	//if (User::$id !== 1)
	//	return "error";
	
	if (empty($_REQUEST['email']))
	{
		ControlAPI::setError("email не заполнен");
		return false;
	}
	
	$user = User::getByMail($_REQUEST['email']);
	$account = $user['id'];
	
	if ($account > 0)
	{
		$Billing=new Billing();
		return $Billing->addOperation($account,$_REQUEST['summ'],$_REQUEST['descr']);
	}
	
	ControlAPI::setError("Не найден аккаунт");
	return false;
}
