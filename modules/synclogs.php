<?php

function synclogsControlAPIMethods()
{
	return array('getSyncQueue','getSyncResults');
}

function synclogsModuleName()
{
	return 'Логи синхронизации';
}


function getSyncQueue()
{
	
	unset($_REQUEST['filter']['menu']);
	
	$DB=DB::getDB();
	
	$sql='
	select SQL_CALC_FOUND_ROWS
		*
	from '.TABLEPREFIX.'varas_queue
	'.Tools::atables_where().'
	'.Tools::atables_orderBy().'
	limit '.$_REQUEST['page']*$_REQUEST['limit'].','.$_REQUEST['limit'];
	
	$arr['rows']=$DB->qry2arr($sql);
	
	foreach($arr['rows'] as &$r)
	{
		$r['data'] = unserialize($r['data']);
	}
	
	$arr['found_rows']=$DB->fr();
	$arr['columns']=$DB->columns(TABLEPREFIX.'a_user');
	$arr['sql']=$sql;
	
	return $arr;
	
}


function getSyncResults()
{
	unset($_REQUEST['filter']['menu']);
	
	$DB=DB::getDB();
	
	$sql='
	select SQL_CALC_FOUND_ROWS
		*
	from '.TABLEPREFIX.'varas_log
	'.Tools::atables_where().'
	'.Tools::atables_orderBy().'
	limit '.$_REQUEST['page']*$_REQUEST['limit'].','.$_REQUEST['limit'];

	$arr['rows']=$DB->qry2arr($sql);
	$arr['found_rows']=$DB->fr();
	$arr['columns']=$DB->columns(TABLEPREFIX.'a_user');
	$arr['sql']=$sql;
	
	return $arr;
	
}



//~ function getSyncQueueData()
//~ {
	//~ 
	//~ $DB=DB::getDB();
	//~ 
	//~ $sql='
	//~ select *
	//~ from d_varas_queue vq
	//~ where id='.(int)$_GET['id'];
	//~ 
	//~ $arr=$DB->qry2arr0($sql);
	//~ $data=unserialize($arr['data']);
	//~ unset($arr['data']);
	//~ $arr = array_merge($arr,$data);
	//~ foreach($arr as $k=>$v)
	//~ {
		//~ $arr[$k]=htmlspecialchars($v);
	//~ }
	//~ return $arr;
	//~ 
//~ }
