<?php

function commentsControlAPIMethods()
{
	return array('getComments','getComment','saveComment','delComment');
}

function commentsModuleName()
{
	return 'Комментарии';
}

function commentsAdditionalControllers()
{
	return array('comment');
}


function getComments()
{
	
	$Comments = new Comments();
	return $Comments->getRows();
	
}

function getComment()
{
	
	$Comments = new Comments();
	return $Comments->get($_REQUEST['id']);
	
}

function saveComment()
{
	
	$Comments = new Comments();
	return $Comments->edit($_REQUEST['data']);
	
}

function delComment()
{
	
	$Comments = new Comments();
	return $Comments->del($_REQUEST['id']);
	
}
