<?php

function marketxmlControlAPIMethods()
{
	return array('getMarketXMLStartValues','getMarketXMLSets','getMarketXMLSet','getMarketXMLFormats','saveMarketXMLSet');
}


function marketxmlModuleName()
{
	return 'MarketXML';
}


function getMarketXMLStartValues()
{
	$result['formats'] = getMarketXMLFormats();
	$result['sets'] = getMarketXMLSets();
	$result['default_params'] = getDefaultParams();
	return $result;
}

function getDefaultParams()
{
	$MXML = new MarketXML();
	return $MXML->getDefaultParams();
}

function getMarketXMLFormats()
{
	$MXML = new MarketXML();
	return $MXML->getFormats();
}

function getMarketXMLSets()
{
	$MXML = new MarketXML();
	return $MXML->getSets();
}

function getMarketXMLSet()
{
	$MXML = new MarketXML();
	return $MXML->getSet($_REQUEST['params']);
}


function saveMarketXMLSet()
{
	$data = $_REQUEST['data'];
	
	$MXML = new MarketXML();
	
	if ((int)$data['id']==0 && $MXML->addSet($data))
	{
		return getMarketXMLSets();
	}
	
	if ($data['id']>0 && $MXML->saveSet($data))
	{
		return getMarketXMLSets();
	}
	
	return false;
	
}


