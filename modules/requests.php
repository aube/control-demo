<?php

function requestsControlAPIMethods()
{
	return array('getRequests','getRequest','editRequest');
}


function requestsModuleName()
{
	return 'Запросы';
}


function getRequests(){
	$R=new Requests();
	return $R->getList();
}


function getRequest(){
	$R=new Requests();
	return $R->get($_REQUEST['id']);
}


function editRequest(){
	//include_once(PROJECT.'/classes.old/class_requests.php');
	$R=new Requests();
	return $R->edit($_REQUEST) ? array('success'=>'complete') : array('error'=>'!!!');
}
