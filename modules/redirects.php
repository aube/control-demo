<?php

function redirectsControlAPIMethods()
{
	return array('getRedirects','delRedirect','saveRedirect');
}

function redirectsModuleName()
{
	return 'Переадресация';
}


function getRedirects()
{
	
	$R=new Redirect();
	return $R->getList();
	
}


function saveRedirect()
{
	
	$R=new Redirect();
	$method = (int)$_REQUEST['method'];
	$method = $method?$method:301;
	$result = $R->add($from=$_REQUEST['from'],$to=$_REQUEST['to'],$method);
	return $result ? array('error'=>'Переадресация установлена') : array('error'=>'Ошибка переадресации');
	
}


function delRedirect()
{
	
	$R=new Redirect();
	return $R->delByHash($_REQUEST['md5_from']);
	
}
