<?php

function adminControlAPIMethods()
{
	return array('imagesSortAllTestAndRestore','restorePathsByParent','restoreChildsByParent','copyPages','deletePages','getErrorLog','getPhpini');
}

function adminModuleName()
{
	return 'Администрирование';
}


function getErrorLog()
{
	$path = ROOT.'/logs/error.log';
	$e = shell_exec('tail -100 '.$path);
	$e = trim($e);
	$e = explode(PHP_EOL,$e);
	return $e;
}



function getPhpini()
{
	return ini_get_all();
}



function imagesSortAllTestAndRestore()
{
	$Images = new Images('');
	$result = $Images->sortAllTestAndRestore();
	
	$result_txt = array();
	foreach($result as $r)
	{
		$restore  = '';
		if (isset($r['restore']))
			$restore = ($r['restore']?', restore:Success':', restore:Failed');
		
		$test = ($r['test']?', OK':', Failed');
		
		$result_txt[] = $r['type'].'/'.$r['subdir'].', test:'.$test.$restore;
	}
	
	return array('success' => 'Проверка завершена','result_txt'=>$result_txt);
}



function restorePathsByParent()
{
	
	$Pages = new Pages();
	return  $Pages->restorePathsByParent($parent = 1);
	
}




function restoreChildsByParent()
{
	
	$Pages = new Pages();
	return  $Pages->restoreChildsByParent($id = 1);
	
}



function copyPages()
{
	
	$Pages = new Pages();
	
	$from = $_REQUEST['from'];
	$to = $_REQUEST['to'];
	
	if (empty($from) || empty($to))
	{
		Errors::set('empty settings');
		return false;
	}
	
	$newParent = $Pages->getPageByURL($to);
	if (!$newParent)
	{
		Errors::set('destination page not found');
		return false;
	}
	
	if (substr($from,-2) == '/*')
	{
		$from = substr($from,0,-2);
		$childs = true;
	}
	if (substr($from,-1) == '/')
	{
		$from = substr($from,0,-1);
		$childs = true;
	}
	
	if ($from == $to)
	{
		Errors::set('error settings');
		return false;
	}
	
	
	$page = $Pages->getPageByURL($from);
	if (!$page)
	{
		Errors::set('copy page not found');
		return false;
	}
	
	$Pages->restorePathsByParent($page['parent']);
	
	
	if ($childs)
	{
		$pages = $Pages->getPagesByParams(array('parent'=>$page['id']));
	}
	else
	{
		$pages[] = $page;
	}
	
	$log = array();
	if (count($pages))
		foreach($pages as $page)
		{
			$result = $Pages->copy($page, $newParent);
			if ($result)
				$log += $result;
		}
	
	return $log;
	
	
}



function deletePages()
{
	
	$Pages = new Pages();
	
	$delete_path = $_REQUEST['delete_path'];
	
	if (empty($delete_path))
	{
		Errors::set('empty settings');
		return false;
	}
	
	if (substr($delete_path,-2) == '/*')
	{
		$delete_path = substr($delete_path,0,-2);
		$parent = $Pages->getPageByURL($delete_path);
	}
	if (substr($delete_path,-1) == '/')
	{
		$delete_path = substr($delete_path,0,-1);
		$parent = $Pages->getPageByURL($delete_path);
	}
	
	if ($parent)
	{
		$pages = $Pages->getPagesByParams(array('parent'=>$parent['id']));
	}
	else
	{
		$pages[] = $Pages->getPageByURL($delete_path);
	}
	
	if (count($pages))
		foreach($pages as $page)
		{
			if (!$Pages->disconnect_all($page['parent'], $page['type'], $page['idtype']))
				return false;
		}
	
	return true;
	
	
}


