<?php

function yandexkassaControlAPIMethods()
{
	return array('getYKPayments','returnYKPayment');
}


function yandexkassaModuleName()
{
	return 'Платежи - Яндекс.Касса';
}


function getYKPayments(){
	$YandexKassa=new YandexKassa();
	$payments = $YandexKassa->getList();
	
	foreach($payments['rows'] as &$payment)
	{
		$payment['check_params'] = unserialize($payment['check_params']);
		$payment['aviso_params'] = unserialize($payment['aviso_params']);
		ob_start();
		var_dump($payment['check_params']);
		$payment['check_params'] = ob_get_contents();
		ob_clean();
		ob_start();
		var_dump($payment['aviso_params']);
		$payment['aviso_params'] = ob_get_contents();
		ob_clean();
	}
	
	return $payments;
	
}

function returnYKPayment(){
	$YandexKassa=new YandexKassa();
	$result = $YandexKassa->returnPayment($_REQUEST['id']);
	
	if ($result)
	{
		return array('message'=>'Платёж возвращен');
	}
	return array('error'=>'Ошибка при возврате платежа');
	
}
