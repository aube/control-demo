<?php

function billingbalanceControlAPIMethods()
{
	return array('getBalance','getOperations');
}


function billingbalanceModuleName()
{
	return 'Биллинг - остатки на счетах';
}


function getBalance(){
	$Billing=new Billing();
	return $Billing->getBalanceList();
}


function getOperations(){
	
	//if (User::$id !== 1)
	//	return "error";
	
	$Billing=new Billing();
	return $Billing->getOperations($_REQUEST['account']);
}
