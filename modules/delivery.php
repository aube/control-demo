<?php

function deliveryControlAPIMethods()
{
	return array('getDeliveryCourier','getDeliveryPickup','addDelivery','delDelivery','editDelivery','updatePickupPoints','getDeliveryPrices');
}

function deliveryModuleName()
{
	return 'Доставка заказов';
}


function getDeliveryCourier()
{
	$Delivery = new Delivery();
	
	$result = $Delivery->getCourierRows();

	return $result;
	
}

function getDeliveryPickup()
{
	
	$Delivery = new Delivery();
	
	$result = $Delivery->getPickupRows();
	
	return $result;
	
}


function updatePickupPoints()
{
	
	$Delivery = new Delivery();
	$result = $Delivery->updatePickupPoints();
	
	if ($result)
	{
		$resultText = 'Пункты самовывоза Hermes-DPD: ';
		if ($result['added'])
			$resultText.= ' Обновлено - '.$result['added'].'';
		if ($result['updated'])
			$resultText.= ' Обновлено - '.$result['updated'].'';
	}
	
	return array('message'=>''.$resultText);
	
}


function delDelivery()
{
	$Delivery = new Delivery();
	return $Delivery->delDelivery($_REQUEST['devid'],$_REQUEST['devtype']);
}


function getDeliveryPrices()
{
	$Delivery = new Delivery();
	return $Delivery->getDeliveryPrices($_REQUEST['devid'],$_REQUEST['devtype']);
}


function _deliveryDataPrepare()
{

	if ($_REQUEST['type'] == 'courier')
	{
		$data = array(
			'city'=>trim($_REQUEST['city']),
			'name'=>trim($_REQUEST['name']),
			'description'=>trim($_REQUEST['description']),
			'large'=>(int)$_REQUEST['large']
		);
		
	}
	else
	{
		$data = array(
			'city'=>trim($_REQUEST['city']),
			'name'=>trim($_REQUEST['name']),
			'region'=>trim($_REQUEST['region']),
			'district'=>trim($_REQUEST['district']),
			'station'=>trim($_REQUEST['station']),
			'metro'=>trim($_REQUEST['metro']),
			'latitude'=>trim($_REQUEST['latitude']),
			'longitude'=>trim($_REQUEST['longitude']),
			'max_size'=>(int)$_REQUEST['max_size'],
			'max_value'=>(int)$_REQUEST['max_value'],
			'max_weight'=>(int)$_REQUEST['max_weight'],
			'zipcode'=>(int)$_REQUEST['zipcode'],
			'address'=>trim($_REQUEST['address']),
			'worktime'=>trim($_REQUEST['worktime'])
		);
	}
	return $data;
}


function addDelivery()
{
	
	$type = $_REQUEST['type'];
	
	$Delivery = new Delivery();
	
	if ($type == 'courier')
	{
		$data = _deliveryDataPrepare();
		$id = $Delivery->addCourierDelivery($data);
	}
	else
	{
		$data = _deliveryDataPrepare();
		$id = $Delivery->addPickupPoint($data);
	}
	
	$prices = $_REQUEST['prices'];
	
	$Delivery->editDeliveryPrices($id,$type,$prices);
	
}


function editDelivery()
{
	
	$id = $_REQUEST['id'];
	$type = $_REQUEST['type'];
	
	$Delivery = new Delivery();
	
	if ($type == 'courier')
	{
		$data = _deliveryDataPrepare();
		$Delivery->editCourierDelivery($id,$data);
	}
	else
	{
		$data = _deliveryDataPrepare();
		$Delivery->editPickupPoint($id,$data);
	}
	
	$prices = $_REQUEST['prices'];
	
	$Delivery->editDeliveryPrices($id,$type,$prices);
	
}
