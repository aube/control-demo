<?php
function typeControlAPIMethods()
{
	return array('getResourse','uploadTypeImg','saveResource','savePage',
				'imageSetMain','imageSave','imageSort','imageRemove','setMainImage',
				'delResource','delPage','getLinked');
}

function typeModuleName()
{
	return 'Ресурс';
}




function getLinked()
{
	
	$Types = new types();
	$Types->type = $_REQUEST['type'];
	
	//on open modale
	if (isset($_REQUEST['current']))
	{
		$idtypes = explode(',',$_REQUEST['current']);
		if (count($idtypes))
		{
			$params['idtype'] = array('in',$idtypes);
			$result = $Types->getResourses($params);
			return $result['rows'];
		}
	}
	
	//on search
	$search = $_REQUEST['search'];
	if (!empty($search))
	{
		
		$params['idtype']=array('like',$search.'%');
		
		if ($Types->type=='item')
			$params['article']=array('like','%'.$search.'%');
		
		$params['header']=array('like','%'.$search.'%');
		
		$params = array($params);
		$params['limit'] = 10;
		$result = $Types->getResourses($params);
		
		return $result['rows'];
	}
	
}



function getResourse()
{
	
	if (class_exists($_REQUEST['type']))
	{
		$type = $_REQUEST['type'];
	}
	
	$Pages = new Pages(false);
	$Type = new $type($_REQUEST);
	$resource = $Type->getByIdtype($_REQUEST['idtype'], false);
	
	//set filters for comments
	$_REQUEST['filter']['type'] = $type;
	$_REQUEST['filter']['idtype'] = $_REQUEST['idtype'];
	$Comments = new Comments();
	$comments = $Comments->getRows();
	
	//изображение по умолчанию нужно убрать из списка
	$imagesParams = $Type->getImagesParams();
	unset($imagesParams['']);
	unset($imagesParams['default']);
	
	return array(
		'typeName'=>$Type->typeName,
		'resource'=>$resource,
		'pages'=>$Pages->getPagesByIdtype($type, $_REQUEST['idtype']),
		'type_fields'=>$Type->getTableFields(),
		'images_params'=>$imagesParams,
		'images'=>$Type->getImages(),
		'upload_max_filesize'=>ini_get('upload_max_filesize'),
		'comments'=>$comments['rows'],
		'comments_found_rows'=>$comments['found_rows']
	);
	
}



function saveResource()
{
	
	if (!class_exists($_REQUEST['type']))
	{
		return false;
	}
	
	$type = $_REQUEST['type'];
	$Type = new $type($_REQUEST['params']);
	
	return $Type->save($_REQUEST['params']);
}


function savePage()
{
	$Pages = new Pages();
	return $Pages->save($_REQUEST['id'], $_REQUEST['params']);
}



function delResource()
{
	
	if (!class_exists($_REQUEST['type']))
	{
		return false;
	}
	
	$type = $_REQUEST['type'];
	$Type = new $type();
	$Type->idtype = $_REQUEST['idtype'];
	
	
	return $Type->del();
}


function delPage()
{
	
	$page = $_REQUEST['page'];
	
	$Pages = new Pages();
	
	return $Pages->disconnect($page['parent'],$page['type'],$page['idtype']);
}




//вызывается из TypeCtrl.uploader()
function uploadTypeImg()
{
	
	ini_set('memory_limit', '512M');
	
	
	$type = $_REQUEST['type'];
	$Type = new $type($_REQUEST);
	
	if (!$Type)
	{
		Errors::set('Type not found');
		return false;
	}
	
	if (!$Type->imagesUpload())
	{
		Errors::set('Upload error');
		return false;
	}
	//ставим последнюю загруженную картинку главной
	//$_REQUEST['id'] = $type['id'];
	//$_REQUEST['image'] = $type['images'][count($type['images'])-1];
	//setMainImage();
	
	return $Type->getImages();
}



function imageSort()
{
	
	$id = (int)$_REQUEST['id'];
	$newPosition = (int)$_REQUEST['newPosition'];
	
	if (!$id) return;
	
	$Images = new Images($_REQUEST['type'], $_REQUEST['idtype']);
	$Images->sort($id, $newPosition);
	
	return $Images->getImages();
}

function imageRemove()
{
	
	$id = (int)$_REQUEST['id'];
	if (!$id) return;
	
	$Images = new Images($_REQUEST['type'], $_REQUEST['idtype']);
	$Images->remove($id);
	
	return $Images->getImages();
	
}


function imageSave()
{
	$id = (int)$_REQUEST['id'];
	if (!$id) return;
	
	//картинки в ассоциативный массив
	$data = array();
	$data['alt'] = $_REQUEST['alt'];
	$data['link'] = $_REQUEST['link'];
	$data['tags'] = Tools::tagsCheck($_REQUEST['tags']);
	
	$Images = new Images($_REQUEST['type'], $_REQUEST['idtype']);
	
	return $Images->edit($id, $data);
	
}


function imageSetMain()
{
	
	$type = new $_REQUEST['type']($_REQUEST);
	return $type->save($_REQUEST);
	
}

