<?php


function defaultControlAPIMethods()
{
	return array('getInfo');
}

function defaultModuleName()
{
	return SITENAME;
}





function getInfo()
{
	$todayStart = date(MYSQL_DATE,strtotime('now 00:00:00'));
	
	
	$Pages = new Pages();
	
	$Requests = new Requests();
	$requestsAll = $Requests->statistics();
	$requestsToday = $Requests->statistics($todayStart);
	$requests = array(
		'today' => (int)$requestsToday[0]['amount'], 
		'all' => (int)$requestsAll[0]['amount'] 
	);
	
	$Comments = new Comments();
	$commentsAll = $Comments->statistics();
	$commentsToday = $Comments->statistics($todayStart);
	$comments = array(
		'today' => (int)$commentsToday[0]['amount'], 
		'all' => (int)$commentsAll[0]['amount'] 
	);
	
	$Users = new Users();
	$usersAll = $Users->statistics();
	$usersToday = $Users->statistics($todayStart);
	$users = array(
		'today' => (int)$usersToday[0]['amount'], 
		'all' => (int)$usersAll[0]['amount'] 
	);
	
	$Orders = new Orders();
	$ordersAll = $Orders->statistics();
	$ordersToday = $Orders->statistics($todayStart);
	$orders = array(
		'today' => (int)$ordersToday[0]['amount'], 
		'all' => (int)$ordersAll[0]['amount'] 
	);
	
	$Stat = new Statistics();
	$widgets = array(
		'server'=>$Stat->getServerInfo()
		,'orders'=>$orders
		,'pages'=>$Pages->statistics()
		,'requests'=>$requests
		,'comments'=>$comments
		,'users'=>$users
	);
	
	//~ return array(
		//~ 'widgets'=>$widgets
		//~ ,'comments'=>$Comments->getList(array('limit'=>5))
		//~ ,'requests'=>$Requests->getList($limit=5)
		//~ ,'orders'=>$Orders->getOrders($limit=5)
	//~ );
	
	
	
	$comments = $Comments->getList(array('limit'=>5));
	//$comments = $comments['rows'];
	
	$requests = $Requests->getList($limit=5);
	$requests = $requests['rows'];
	
	$orders = $Orders->getOrders($limit=5);
	//$orders = $orders['rows'];//?
	
	return array(
		'widgets'=>$widgets
		,'comments'=>$comments
		,'requests'=>$requests
		,'orders'=>$orders
	);
}
