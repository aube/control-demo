<?php

function ordersControlAPIMethods()
{
	return array('getOrders','getOrder','setOrderPrepay','clearOrderPrepay');
}


function ordersModuleName()
{
	return 'Заказы';
}

function ordersAdditionalControllers()
{
	return array('order');
}

function getOrders()
{
	
	
	$where = Tools::atables_where($prefixe);
	$where = !$where ? 'where status<>"tpl"' : $where.' and status<>"tpl"';
	
	$order = Tools::atables_orderBy();
	
	$Orders = new Orders();
	$Orders->table = TABLEPREFIX . Orders::TABLE;
	
	$sql='select *,substring(comment,1,5) short_comment
	FROM `'.$Orders->table.'`
	'.$where.'
	'.$order.'
	limit '.$_REQUEST['page']*$_REQUEST['limit'].','.$_REQUEST['limit'];
	//pr($sql);
	$shop=getV('shop');
	
	$currencyName=$shop['settings']['currensyName'];
	
	$arr=select_fr($sql);
	
	//$arr['found_rows'] = select_fr();
	
	$arr['columns'] = Tools::atables_columns($Orders->table);
	$arr['sql'] = $sql;
	
	return $arr;
}


function getOrder()
{
	
	
	$Order = new Order();
	$order = $Order->get((int)$_REQUEST['id']);
	$order['items'] = $Order->getItems((int)$_REQUEST['id']);
	
	$shop=getV('shop');
	$currencyName=$shop['settings']['currensyName'];
	
	$order['recipient'] .= ($order['user_id']==0?' [не зарегистрирован]':'');
	$order['name']=$order['recipient'];
	$order['mail']=$order['user_mail'];
	$order['summ']=(float)$order['summ'];
	$order['delivery_cost']=(float)$order['delivery_cost'];
	$order['prepay']=(float)$order['prepay'];
	$order['coupon_discount']=(float)$order['coupon_discount'];
	
	$order['total']=bcsub(
			bcadd($order['summ'],$order['delivery_cost'],2),
			bcadd($order['prepay'],$order['coupon_discount'],2),
		2);
	
	
	$order['account_balance'] = '';
	if ($order['user_id'])
	{
		$Billing=new Billing();
		$order['account_balance']=$Billing->getBalance($order['user_id']);
	}
	
	
	return $order;
}


function setOrderPrepay()
{
	
	$id = (int)$_REQUEST['id'];
	if (!$id) return false;
	
	
	$Order = new Order();
	$Order->admin = true;
	$order = $Order->get($id);
	
	if ($order['prepay'] > 0)
	{
		return array('error'=>'По заказу проведена предоплата');
	}
	
	$total = $Order->getTotal($order);
	
	$Billing=new Billing();
	$account_ballance = $Billing->getBalance($order['user_id']);
	
	if ($account_ballance < $total)
	{
		return array('error'=>'Сумма на счете пользователя недостаточна для предоплаты');
	}
	
	$user_id = (int)$order['user_id'];
	if ($user_id>0)
	{
		$Order->edit($id,array('prepay'=>$total));
		$Billing->addOperation($user_id, -$total, "Установка предоплаты по заказу #".$id, User::$id);
	}
	else
	{
		return array('error'=>'Заказ без регистрации');
	}
}


function clearOrderPrepay()
{
	
	$id = (int)$_REQUEST['id'];
	if (!$id) return false;
	
	
	$Order = new Order();
	$Order->admin = true;
	$order = $Order->get($id);
	
	if ($order['prepay'] == 0)
	{
		return array('error'=>'По заказу нет предоплаты');
	}
	
	$Billing=new Billing();
	$user_id = (int)$order['user_id'];
	if ($user_id>0)
	{
		$Order->edit($id,array('prepay'=>0));
		$Billing->addOperation($user_id, $order['prepay'], "Возврат предоплаты по заказу #".$id, User::$id);
	}
	else
	{
		return array('error'=>'Заказ без регистрации');
	}
	
}
