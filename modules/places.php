<?php

function placesControlAPIMethods()
{
	return array('getPlace','delPlace','savePlace','getPlaces');
}

function placesModuleName()
{
	return 'Представительства, Партнёры, Торговые точки';
}

function placesAdditionalControllers()
{
	return array('place');
}

function getPlaces()
{
	
	$Place = new Place();
	
	if ($_REQUEST['filter'])
		foreach($_REQUEST['filter'] as $k=>$v)
		{
			$params[$k] = array('like','%'.$v.'%');
		}
	
	
	$params['limit'] = $_REQUEST['limit'];
	$params['page'] = $_REQUEST['page'];
	
	$result = $Place->getList_fr($params);

	return $result;
	
}


function savePlace()
{
	
	$params = $_REQUEST['params'];
	$id = $params['id'];
	unset($params['id']);
	
	$Place = new Place();
	
	if ($id)
	{
		$result = $Place->edit($id, $params);
	}
	else
	{
		$result = $Place->add($params);
	}
	
	return $result;
	
}


function delPlace()
{
	$params = $_REQUEST['params'];
	$id = $params['id'];
	unset($params['id']);
	
	$Place = new Place();
	
	$result = $Place->del($id);

	return $result;
	
}


function getPlace()
{
	$id = $_REQUEST['id'];
	
	$Place = new Place();
	
	$result = $Place->get($id);

	return $result;
	
}
