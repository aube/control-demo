<?php

function statichtmlControlAPIMethods()
{
	return array('getStaticHTMLFiles','saveStaticHTMLFile');
}

function statichtmlModuleName()
{
	return 'Статичный HTML';
}



function getStaticHTMLFiles()
{
	$HTML = new HTML();
	return $HTML->getStaticHTMLFiles();
}



function saveStaticHTMLFile()
{
	$dir = PROJECT.'/templates/html/';
	$file = $_REQUEST['file'];
	
	file_put_contents($dir.$file, $_REQUEST['content']);
}


