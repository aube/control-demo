<?php

function statisticsControlAPIMethods()
{
	return array('getStatistics');
}

function statisticsModuleName()
{
	return 'Статистика';
}



function getStatistics()
{
	$Stat = new Statistics();
	
	$allorders = $Stat->getAllOrders();
	$allusers = $Stat->getAllUsers();
	$allrequests = $Stat->getAllRequests();
	
	$orders = $Stat->getOrders();
	$users = $Stat->getUsers();
	$metrics = $Stat->getMetrics();
	
	$_REQUEST['limit']=3; //for $requests
	$_REQUEST['order']='dt-'; //for $requests
	$requests = getRequests();
	
	return array(
		'allorders'=>$allorders,
		'allusers'=>$allusers,
		'allrequests'=>$allrequests,
		'orders'=>$orders,
		'users'=>$users,
		'requests'=>$requests,
		'server'=>$Stat->getServerInfo(),
		'metrics'=>array(
			'name'=>'Статистика посещений',
			'content'=>$metrics
			),
		);
}
