<?php

function treeControlAPIMethods()
{
	return array('getTree','getNode','getNodesByHeader','getListChilds','getResoursesToLink','connectPage','disconnectPage','sortPosition','sortRelease','sortTop');
}

function treeModuleName()
{
	return 'Страницы сайта';
}


function _paramsPreparation()
{
	
	$params = array();
	
	if ((int)$_REQUEST['limit'])
		$params['limit'] = (int)$_REQUEST['limit'];
	
	if ((int)$_REQUEST['page'])
		$params['page'] = (int)$_REQUEST['page'];
	
	if ((int)$_REQUEST['parent'])
		$params['parent'] = (int)$_REQUEST['parent'];
	
	
	if($_REQUEST['filter']['parent'])
	{
		$params['parent'] = $_REQUEST['filter']['parent'];
		unset($_REQUEST['filter']['parent']);
	}
	
	return $params;
}



function getTree()
{
	
	$Pages = new Pages();
	
	$id = (int)$_REQUEST['id'];
	$page = $Pages->getPageById($id);
	$activeNode = $page;
	
	$params['limit'] = 1000;
	$params[1] = array(
		'childs' => array('>',0),
		'type' => array('like','%f')
		);
	$params['parent'] = $page['id'];
	$page['nodes'] = $Pages->getPages($params);
	$page['nodes'] = $page['nodes']['rows'];
	$page['open'] = true;
	
	while($page['parent']>0)
	{
		$tmp = $Pages->getPageById($page['parent']);
		
		$params['parent'] = $tmp['id'];
		$tmp['nodes'] = $Pages->getPages($params);
		$tmp['nodes'] = $tmp['nodes']['rows'];
		
		foreach($tmp['nodes'] as &$node)
		{
			if ($node['id'] == $page['id'])
			{
				$node['nodes'] = $page['nodes'];
				$node['open'] = true;
				break;
			}
			
		}
		$page = $tmp;
		
	}
	
	return array('tree'=>$page,'activeNode'=>$activeNode);
}


function getNode()
{
	
	$Pages = new Pages();
	
	$id = (int)$_REQUEST['id'];
	$page = $Pages->getPageById($id);
	
	
	$params['limit'] = 100;
	$params[1] = array(
		'childs' => array('>',0),
		'type' => array('like','%f')
		);
	$params['parent'] = $id;
	
	$pages = $Pages->getPages($params);
	$page['nodes'] = $pages['rows'];
	
	return $page;
}



function getNodesByHeader()
{
	$header = $_REQUEST['header'];
	$Pages = new Pages();
	$page = $Pages->getPageById($id);
	
	//$_REQUEST['type'] = $page['type'];
	//$page['slave_types'] = getSlaveTypes();
	
	return $page;
}


function getListChilds()
{
	
	$params = _paramsPreparation();
	
	if($_REQUEST['filter'])
	foreach($_REQUEST['filter'] as $k=>$v)
	{
		$params[$k] = array('like','%'.$v.'%');
	}
	
	$Pages = new Pages(false);
	$pages = $Pages->getPages($params);
	
	$Pages = new Pages();
	$page = $Pages->getPageById($params['parent']);
	
	$_REQUEST['type'] = $page['type'];
	//$pages['slave_types'] = getSlaveTypes();
	
	return $pages;
	
}



function getResoursesToLink()
{
	
	
	//запрос только приклеенных
	if($_REQUEST['onlyActiveNodeResourses'])
	{
		$types = getListChilds();
		foreach($types['rows'] as &$type)
		{
			$type['linked']=1;
		}
		return $types;
	}
	
	
	
	//save parent for linked select
	$parent = (int)$_REQUEST['filter']['parent'];
	
	$params = _paramsPreparation();
	unset($params['parent']);
	
	
	if($_REQUEST['filter'])
	foreach($_REQUEST['filter'] as $k=>$v)
	{
		$k = ($k=='type'?'':'tp.').$k;
		$params[$k] = array('like','%'.$v.'%');
	}
	
	if($_REQUEST['onlyNotConnectedResourses'])
		$params['onlyNotConnectedResourses'] = $_REQUEST['onlyNotConnectedResourses'];
	
	
	$Types = new Types();
	$params['types'] = $Types->getSlaveTypes($_REQUEST['type']);
	
	
	$Types = new Types(false);
	$types = $Types->getResoursesMultiTypes($params);
	
	//set slave_types
	$types['slave_types'] = $params['types'];
	
	if ($types['found_rows']==0)
	{
		return $types;
	}
	
	
	//дальнейшая канитель ради установки результатам поиска флага привязки к ЧПУ
	//ибо группировать объединенный запрос getResoursesMultiTypes слишком геморно
	foreach($types['rows'] as &$type)
	{
		$type['linked']=0;
		$idtypes[] = $type['idtype'];
	}
	
	
	$Pages = new Pages(false);
	$params['parent'] = $parent;
	$params['idtype'] = array('in',$idtypes);
	
	if($_REQUEST['filter'])
	foreach($_REQUEST['filter'] as $k=>$v)
	{
		unset($params['tp.'.$k]);
		$params[$k] = array('like','%'.$v.'%');
	}
	
	unset($params['types']);
	unset($params['page']);
	unset($params['onlyNotConnectedResourses']);
	
	$linked = $Pages->getPages($params);
	if ($linked['rows'])
	foreach($linked['rows'] as &$link)
	{
		foreach($types['rows'] as &$type)
		{
			
			if ($type['type'] == $link['type'] && $type['idtype'] == $link['idtype'])
			{
				
				$type['linked']=1;
				
				break;
			}
		}
	}
	
	return $types;
	
	
}



//sorts
function sortTop()
{
	$Pages = new Pages(false);
	return $Pages->sortTop($_REQUEST['id'], $_REQUEST['parent']);
}

function sortRelease()
{
	$Pages = new Pages(false);
	return $Pages->sortRelease($_REQUEST['id'], $_REQUEST['parent']);
}

function sortPosition()
{
	$Pages = new Pages(false);
	return $Pages->sortPosition($_REQUEST['id'], $_REQUEST['parent'], $_REQUEST['after']);
}



//connections
function connectPage()
{
	
	$type = $_REQUEST['type'];
	$idtype = $_REQUEST['idtype'];
	$parent = $_REQUEST['parent'];
	$alias = $_REQUEST['alias'];
	
	$Pages = new Pages(false);
	return $Pages->connect($type, $idtype, $parent, $alias);
	
}
function disconnectPage()
{
	
	$parent = $_REQUEST['parent'];
	$idtype = $_REQUEST['idtype'];
	$type = $_REQUEST['type'];
	
	$Pages = new Pages(false);
	return $Pages->disconnect($parent, $type, $idtype);
	
}

