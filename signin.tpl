<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>LS control panel</title>

		<!-- Bootstrap core CSS -->
		<link href="./_css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="./_css/signin.css" rel="stylesheet">

	</head>

	<body>
		<div class="container">
			
			<form role="form" method="post">
				<input name="redirect" value="/<?=CONTROL_URL?>" type="hidden">
				
				<div class="row">
					<div class="col-xs-6 col-xs-offset-3">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">@</div>
								<input class="form-control" placeholder="Электронная почта" name="mail" value="" type="email">
							</div>
						</div>
						
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
								<input class="form-control" placeholder="Пароль" name="pass" value="" type="password">
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-6">
							</div>
							
							<div class="col-xs-6 text-right">
									<button type="submit" class="btn btn-success" name="auth">Войти</button>
							</div>
						</div>
						
						<div class="form-group">
							<div class="text-right">
								<br>
								<a href="/reminder" id="reminderTrigger">Забыли пароль?</a>
							</div>
						</div>
						
					</div>
					
				</div>
			</form>
		</div>
	</body>
</html>
